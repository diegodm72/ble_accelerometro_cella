/*******************************************************************************
* File Name: main.c
*
* Version: 1.0
*
* Description:
*  Temperature measurement example project that simulates thermometer readings
*  and sends it over BLE Health Thermometer service.
*
* Note:
*
* Hardware Dependency:
*  CY8CKIT-042 BLE
* 
********************************************************************************
* Copyright 2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "common.h"
#include "hts.h"
#include "bas.h"

#include "lis2dh12.h"
#include "m95020.h"

uint8 busStatus = CYBLE_STACK_STATE_FREE;           /* Status of stack queue */

volatile uint32 mainTimer = 0;

void tickUser();

void AppCallBack(uint32 event, void* eventParam);
void WDT_Start(void);
void WDT_Stop(void);
static void LowPowerImplementation(void);

uint8_t notifica_abilitata = 0;
CYBLE_CONN_HANDLE_T device_connesso;

CYBLE_GATTS_HANDLE_VALUE_NTF_T handleValue;

uint8_t bufftx[20];
uint16_t cella;
int32_t timer_send = 0;

/*******************************************************************************
* Function Name: main()
********************************************************************************
* Summary:
*  Main function for the project.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Theory:
*  The function starts BLE and UART components.
*  This function process all BLE events and also implements the low power 
*  functionality.
*
*******************************************************************************/
int main()
{
    CyGlobalIntEnable;  
    
    /* Start CYBLE component and register generic event handler */
    CyBle_Start(AppCallBack);
    /* Register service specific callback functions */
    
	ADC_Start();
    WDT_Start();
    SPIM_Start();
    
    cs_acc_Write(1);
    cs_ee_Write(1);
    
    fram_init();
    lis2dh12_init();
    
    CySysTickInit();
    CySysTickSetCallback(0,tickUser);
    CySysTickEnable();
    
    /***************************************************************************
    * Main polling loop
    ***************************************************************************/
    
    device_connesso.bdHandle = 0;
    device_connesso.attId    = 0xff;
    
    ADC_StartConvert();
    
	while(1) 
    {   
        /* CyBle_ProcessEvents() allows BLE stack to process pending events */
        CyBle_ProcessEvents();

        /* To achieve low power in the device */
        LowPowerImplementation();
        
        lis2dh12_read_all();
        CyDelay(10);
        
        /***********************************************************************
        * Wait for connection established with Central device
        ***********************************************************************/
        if(CyBle_GetState() == CYBLE_STATE_CONNECTED)
        {           
            if( notifica_abilitata )
            {
                if( lis2dh12_read_all() )
                //if( timer_send == 0)
                {
                    //gapDevice[device].notificationHandle.attrHandle = CY_BLE_SERVER_UART_TX_DATA_CHAR_HANDLE;
                    
                    //CYBLE_GATTS_SendNotification( &gapDevice[device].connHandle, &gapDevice[device].notificationHandle);
                    
                    handleValue.attrHandle = CYBLE_SERVER_UART_TX_DATA_CHAR_HANDLE;
                    handleValue.value.len  = 10;
                    
                    bufftx[0] = 0x01;
                    bufftx[1] = (uint8_t)(cella >> 8);   // cella h
                    bufftx[2] = (uint8_t)(cella & 0xff);
                    bufftx[3] = (uint8_t)(mems.axis_x >> 8); // acc x
                    bufftx[4] = (uint8_t)(mems.axis_x & 0xff);
                    bufftx[5] = (uint8_t)(mems.axis_y >> 8);   // acc y
                    bufftx[6] = (uint8_t)(mems.axis_y & 0xff);
                    bufftx[7] = (uint8_t)(mems.axis_z >> 8);   // acc z
                    bufftx[8] = (uint8_t)(mems.axis_z & 0xff);
                    bufftx[9] = 0;      // opt
                    
                    handleValue.value.val = bufftx;
                    
                    CyBle_GattsNotification(device_connesso, &handleValue);
                    timer_send = 500;
                }       
                if( ADC_IsEndConversion(ADC_RETURN_STATUS) )
                {
                    ADC_StopConvert();
                    cella = ADC_GetResult16(0);
                    ADC_StartConvert();
                }
                
            }            
        }
	}   
}  


void tickUser()
{
    if( timer_send ) --timer_send;
}

/*******************************************************************************
* Function Name: AppCallBack()
********************************************************************************
*
* Summary:
*   This is an event callback function to receive events from the BLE Component.
*
* Parameters:
*  event - the event code
*  *eventParam - the event parameters
*
*******************************************************************************/
void AppCallBack(uint32 event, void* eventParam)
{
    CYBLE_CONN_HANDLE_T connHandle;
    CYBLE_GATTS_WRITE_REQ_PARAM_T* wrReqParam;
  
    CYBLE_API_RESULT_T apiResult;
    CYBLE_GAP_BD_ADDR_T localAddr;
    CYBLE_GAP_AUTH_INFO_T *authInfo;
    uint8 i;

    switch (event)
	{
        /**********************************************************
        *                       General Events
        ***********************************************************/
		case CYBLE_EVT_STACK_ON: /* This event received when component is Started */
            /* Enter in to discoverable mode so that remote can search it. */
            apiResult = CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
            localAddr.type = 0u;
            //CyBle_GetDeviceAddress(&localAddr);
            break;
		case CYBLE_EVT_TIMEOUT: 
        	/* CYBLE_GAP_ADV_MODE_TO - Advertisement time set by application is expired */
    	    /* CYBLE_GAP_AUTH_TO - Authentication procedure timeout */
            /* CYBLE_GAP_SCAN_TO - Scan time set by application is expired */
            /* CYBLE_GATT_RSP_TO - GATT procedure timeout */
			break;
		case CYBLE_EVT_HARDWARE_ERROR:
			break;
        case CYBLE_EVT_HCI_STATUS:
			break;

        /**********************************************************
        *                       GAP Events
        ***********************************************************/
        case CYBLE_EVT_GAP_AUTH_REQ:
//            DBG_PRINTF("CYBLE_EVT_AUTH_REQ: security=%x, bonding=%x, ekeySize=%x, err=%x \r\n", 
//                (*(CYBLE_GAP_AUTH_INFO_T *)eventParam).security, 
//                (*(CYBLE_GAP_AUTH_INFO_T *)eventParam).bonding, 
//                (*(CYBLE_GAP_AUTH_INFO_T *)eventParam).ekeySize, 
//                (*(CYBLE_GAP_AUTH_INFO_T *)eventParam).authErr);
            break;
        case CYBLE_EVT_GAP_PASSKEY_ENTRY_REQUEST:
            break;
        case CYBLE_EVT_GAP_PASSKEY_DISPLAY_REQUEST:
            break;
        case CYBLE_EVT_GAP_AUTH_COMPLETE:
//            authInfo = (CYBLE_GAP_AUTH_INFO_T *)eventParam;
//            (void)authInfo;
//            DBG_PRINTF("AUTH_COMPLETE: security:%x, bonding:%x, ekeySize:%x, authErr %x \r\n", 
//                                    authInfo->security, authInfo->bonding, authInfo->ekeySize, authInfo->authErr);
            break;
        case CYBLE_EVT_GAP_AUTH_FAILED:
            break;
        case CYBLE_EVT_GAPP_ADVERTISEMENT_START_STOP:
            if(CYBLE_STATE_DISCONNECTED == CyBle_GetState())
            {   
                /* Fast and slow advertising period complete, go to low power  
                 * mode (Hibernate mode) and wait for an external
                 * user event to wake up the device again */
//                Advertising_LED_Write(LED_OFF);
//                Disconnect_LED_Write(LED_ON);
//                LowPower_LED_Write(LED_OFF);
//            #if (DEBUG_UART_ENABLED == ENABLED)
//                while((UART_DEB_SpiUartGetTxBufferSize() + UART_DEB_GET_TX_FIFO_SR_VALID) != 0);
//            #endif /* (DEBUG_UART_ENABLED == ENABLED) */
//                SW2_ClearInterrupt();
//                Wakeup_Interrupt_ClearPending();
//                Wakeup_Interrupt_Start();
//                CySysPmHibernate();
                
                apiResult = CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
            }
            break;
        case CYBLE_EVT_GAP_DEVICE_CONNECTED:
//            Advertising_LED_Write(LED_OFF);
            break;
        case CYBLE_EVT_GAP_DEVICE_DISCONNECTED:
//            LowPower_LED_Write(LED_OFF);
            /* Put the device to discoverable mode so that remote can search it. */
            apiResult = CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
            break;
        case CYBLE_EVT_GAP_ENCRYPT_CHANGE:
            break;
        case CYBLE_EVT_GAPC_CONNECTION_UPDATE_COMPLETE:
            break;
        case CYBLE_EVT_GAP_KEYINFO_EXCHNGE_CMPLT:
            break;

        /**********************************************************
        *                       GATT Events
        ***********************************************************/
        case CYBLE_EVT_GATT_CONNECT_IND:
            connHandle = *(CYBLE_CONN_HANDLE_T*)eventParam;
            
            device_connesso.bdHandle = connHandle.bdHandle;
            device_connesso.attId    = connHandle.attId;
            
            break;
        case CYBLE_EVT_GATT_DISCONNECT_IND:
            connHandle = *(CYBLE_CONN_HANDLE_T*)eventParam;
            
            device_connesso.bdHandle = connHandle.bdHandle;
            device_connesso.attId    = 0xff;
            break;
        case CYBLE_EVT_GATTS_WRITE_REQ:
            
            wrReqParam = (CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam;
            
            if (wrReqParam->handleValPair.attrHandle == CYBLE_SERVER_UART_RX_DATA_CHAR_HANDLE)
            {
                //writePar(wrReqParam->handleValPair.value.val, wrReqParam->handleValPair.value.len, wrReqParam->connHandle );
            }
            if (wrReqParam->handleValPair.attrHandle == CYBLE_SERVER_UART_TX_DATA_CLIENT_CHARACTERISTIC_CONFIGURATION_DESC_HANDLE)
            {
                // All data start notifica
                
                
                //gattErr = Cy_BLE_GATTS_WriteAttributeValuePeer(&wrReqParam->connHandle, &wrReqParam->handleValPair);
                
                //CyBle_GattsWriteAttributeValue( wrReqParam->handleValPair.attrHandle ,0,wrReqParam->connHandle,0 );
                
//                if (gattErr != CYBLE_GATT_ERR_NONE)
//                {
//                    /* Send an Error Response */
//                    errInfo.opCode = CYBLE_GATT_WRITE_REQ;
//                    errInfo.attrHandle = wrReqParam->handleValPair.attrHandle;
//                    errInfo.errorCode = gattErr;
//                    Cy_BLE_GATTS_SendErrorRsp(&wrReqParam->connHandle, &errInfo);
//                    break;
//                }
                if (wrReqParam->handleValPair.value.val[0] & CYBLE_CCCD_NOTIFICATION)
                {
                    
                    //gattsDbInfo.connHandle = wrReqParam->connHandle;
                    //gattsDbInfo.handleValuePair
                    
                    notifica_abilitata = 1;
                }
            }
            
            //            ShowValue(&((CYBLE_GATTS_WRITE_REQ_PARAM_T *)eventParam)->handleValPair.value);
            (void)CyBle_GattsWriteRsp(((CYBLE_GATTS_WRITE_REQ_PARAM_T *)eventParam)->connHandle);
            break;
        case CYBLE_EVT_GATTS_INDICATION_ENABLED:
            break;
        case CYBLE_EVT_GATTS_INDICATION_DISABLED:
            break;
        case CYBLE_EVT_GATTS_READ_CHAR_VAL_ACCESS_REQ:
            /* Triggered on server side when client sends read request and when
            * characteristic has CYBLE_GATT_DB_ATTR_CHAR_VAL_RD_EVENT property set.
            * This event could be ignored by application unless it need to response
            * by error response which needs to be set in gattErrorCode field of
            * event parameter. */
//            DBG_PRINTF("CYBLE_EVT_GATTS_READ_CHAR_VAL_ACCESS_REQ: handle: %x \r\n", 
//                ((CYBLE_GATTS_CHAR_VAL_READ_REQ_T *)eventParam)->attrHandle);
            break;

		/**********************************************************
        *                       Other Events
        ***********************************************************/
        case CYBLE_EVT_PENDING_FLASH_WRITE:
            /* Inform application that flash write is pending. Stack internal data 
            * structures are modified and require to be stored in Flash using 
            * CyBle_StoreBondingData() */
            break;

        default:
			break;
	}
}


/*******************************************************************************
* Function Name: Timer_Interrupt
********************************************************************************
*
* Summary:
*  Handles the Interrupt Service Routine for the WDT timer.
*
*******************************************************************************/
CY_ISR(Timer_Interrupt)
{
    if(CySysWdtGetInterruptSource() & WDT_INTERRUPT_SOURCE)
    {
        static uint8 led = LED_OFF;
        
        /* Blink LED to indicate that device advertises */
        if(CYBLE_STATE_ADVERTISING == CyBle_GetState())
        {
            led ^= LED_OFF;
//            Advertising_LED_Write(led);
        }
        
        /* Indicate that timer is raised to the main loop */
        mainTimer++;
        
        /* Clears interrupt request  */
        CySysWdtClearInterrupt(WDT_INTERRUPT_SOURCE);
    }
}

/*******************************************************************************
* Function Name: WDT_Start
********************************************************************************
*
* Summary:
*  Configures WDT to trigger an interrupt every second.
*
*******************************************************************************/

void WDT_Start(void)
{
    /* Unlock the WDT registers for modification */
    CySysWdtUnlock(); 
    /* Setup ISR */
    WDT_Interrupt_StartEx(&Timer_Interrupt);
    /* Write the mode to generate interrupt on match */
    CySysWdtWriteMode(WDT_COUNTER, CY_SYS_WDT_MODE_INT);
    /* Configure the WDT counter clear on a match setting */
    CySysWdtWriteClearOnMatch(WDT_COUNTER, WDT_COUNTER_ENABLE);
    /* Configure the WDT counter match comparison value */
    CySysWdtWriteMatch(WDT_COUNTER, WDT_1SEC);
    /* Reset WDT counter */
    CySysWdtResetCounters(WDT_COUNTER);
    /* Enable the specified WDT counter */
    CySysWdtEnable(WDT_COUNTER_MASK);
    /* Lock out configuration changes to the Watchdog timer registers */
    CySysWdtLock();    
}


/*******************************************************************************
* Function Name: WDT_Stop
********************************************************************************
*
* Summary:
*  This API stops the WDT timer.
*
*******************************************************************************/
void WDT_Stop(void)
{
    /* Unlock the WDT registers for modification */
    CySysWdtUnlock(); 
    /* Disable the specified WDT counter */
    CySysWdtDisable(WDT_COUNTER_MASK);
    /* Locks out configuration changes to the Watchdog timer registers */
    CySysWdtLock();    
}


/*******************************************************************************
* Function Name: LowPowerImplementation()
********************************************************************************
* Summary:
* Implements low power in the project.
*
* Parameters:
* None
*
* Return:
* None
*
* Theory:
* The function tries to enter deep sleep as much as possible - whenever the 
* BLE is idle and the UART transmission/reception is not happening. At all other
* times, the function tries to enter CPU sleep.
*
*******************************************************************************/
static void LowPowerImplementation(void)
{
    CYBLE_LP_MODE_T bleMode;
    uint8 interruptStatus;
    
    /* For advertising and connected states, implement deep sleep 
     * functionality to achieve low power in the system. For more details
     * on the low power implementation, refer to the Low Power Application 
     * Note.
     */
    if((CyBle_GetState() == CYBLE_STATE_ADVERTISING) || 
       (CyBle_GetState() == CYBLE_STATE_CONNECTED))
    {
        /* Request BLE subsystem to enter into Deep-Sleep mode between connection and advertising intervals */
        bleMode = CyBle_EnterLPM(CYBLE_BLESS_DEEPSLEEP);
        /* Disable global interrupts */
        interruptStatus = CyEnterCriticalSection();
        /* When BLE subsystem has been put into Deep-Sleep mode */
        if(bleMode == CYBLE_BLESS_DEEPSLEEP)
        {
            /* And it is still there or ECO is on */
            if((CyBle_GetBleSsState() == CYBLE_BLESS_STATE_ECO_ON) || 
               (CyBle_GetBleSsState() == CYBLE_BLESS_STATE_DEEPSLEEP))
            {
                CySysPmDeepSleep();
            }
        }
        else /* When BLE subsystem has been put into Sleep mode or is active */
        {
            /* And hardware doesn't finish Tx/Rx opeation - put the CPU into Sleep mode */
            if(CyBle_GetBleSsState() != CYBLE_BLESS_STATE_EVENT_CLOSE)
            {
                CySysPmSleep();
            }
        }
        /* Enable global interrupt */
        CyExitCriticalSection(interruptStatus);
    }
}




/* [] END OF FILE */

