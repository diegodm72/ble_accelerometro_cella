/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */

#include <stdint.h>

#define WREN  0b00000110
#define WRDI  0b00000100
#define RDSR  0b00000101
#define WRSR  0b00000001
#define READ  0b00000011
#define WRITE 0b00000010


uint8_t fram_init();

void fram_write_byte( uint8_t address, uint8_t val );
void fram_read_byte(  uint8_t address, uint8_t* val );

uint8_t fram_write_array( uint8_t address, uint8_t* array, uint8_t len );
uint8_t fram_read_array(  uint8_t address, uint8_t* array, uint8_t len );


typedef struct
{
    unsigned WIP :1;
    unsigned WEL :1;
    unsigned BP  :2;
    unsigned NC  :4;
}statusreg_s;

typedef union
{
    uint8_t     val;
    statusreg_s b;
}statusreg_u;
