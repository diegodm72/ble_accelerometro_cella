/*******************************************************************************
* File Name: adc_1_PM.c
* Version 2.50
*
* Description:
*  This file provides Sleep/WakeUp APIs functionality.
*
* Note:
*
********************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "adc_1.h"


/***************************************
* Local data allocation
***************************************/

static adc_1_BACKUP_STRUCT  adc_1_backup =
{
    adc_1_DISABLED,
    0u    
};


/*******************************************************************************
* Function Name: adc_1_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void adc_1_SaveConfig(void)
{
    /* All configuration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: adc_1_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void adc_1_RestoreConfig(void)
{
    /* All configuration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: adc_1_Sleep
********************************************************************************
*
* Summary:
*  Stops the ADC operation and saves the configuration registers and component
*  enable state. Should be called just prior to entering sleep.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  adc_1_backup - modified.
*
*******************************************************************************/
void adc_1_Sleep(void)
{
    /* During deepsleep/ hibernate mode keep SARMUX active, i.e. do not open
    *   all switches (disconnect), to be used for ADFT
    */
    adc_1_backup.dftRegVal = adc_1_SAR_DFT_CTRL_REG & (uint32)~adc_1_ADFT_OVERRIDE;
    adc_1_SAR_DFT_CTRL_REG |= adc_1_ADFT_OVERRIDE;
    if((adc_1_SAR_CTRL_REG  & adc_1_ENABLE) != 0u)
    {
        if((adc_1_SAR_SAMPLE_CTRL_REG & adc_1_CONTINUOUS_EN) != 0u)
        {
            adc_1_backup.enableState = adc_1_ENABLED | adc_1_STARTED;
        }
        else
        {
            adc_1_backup.enableState = adc_1_ENABLED;
        }
        adc_1_StopConvert();
        adc_1_Stop();
        
        /* Disable the SAR internal pump before entering the chip low power mode */
        if((adc_1_SAR_CTRL_REG & adc_1_BOOSTPUMP_EN) != 0u)
        {
            adc_1_SAR_CTRL_REG &= (uint32)~adc_1_BOOSTPUMP_EN;
            adc_1_backup.enableState |= adc_1_BOOSTPUMP_ENABLED;
        }
    }
    else
    {
        adc_1_backup.enableState = adc_1_DISABLED;
    }
}


/*******************************************************************************
* Function Name: adc_1_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component enable state and configuration registers.
*  This should be called just after awaking from sleep mode.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  adc_1_backup - used.
*
*******************************************************************************/
void adc_1_Wakeup(void)
{
    adc_1_SAR_DFT_CTRL_REG = adc_1_backup.dftRegVal;
    if(adc_1_backup.enableState != adc_1_DISABLED)
    {
        /* Enable the SAR internal pump  */
        if((adc_1_backup.enableState & adc_1_BOOSTPUMP_ENABLED) != 0u)
        {
            adc_1_SAR_CTRL_REG |= adc_1_BOOSTPUMP_EN;
        }
        adc_1_Enable();
        if((adc_1_backup.enableState & adc_1_STARTED) != 0u)
        {
            adc_1_StartConvert();
        }
    }
}
/* [] END OF FILE */
