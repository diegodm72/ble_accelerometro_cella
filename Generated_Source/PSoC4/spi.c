/***************************************************************************//**
* \file spi.c
* \version 4.0
*
* \brief
*  This file provides the source code to the API for the SCB Component.
*
* Note:
*
*******************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "spi_PVT.h"

#if (spi_SCB_MODE_I2C_INC)
    #include "spi_I2C_PVT.h"
#endif /* (spi_SCB_MODE_I2C_INC) */

#if (spi_SCB_MODE_EZI2C_INC)
    #include "spi_EZI2C_PVT.h"
#endif /* (spi_SCB_MODE_EZI2C_INC) */

#if (spi_SCB_MODE_SPI_INC || spi_SCB_MODE_UART_INC)
    #include "spi_SPI_UART_PVT.h"
#endif /* (spi_SCB_MODE_SPI_INC || spi_SCB_MODE_UART_INC) */


/***************************************
*    Run Time Configuration Vars
***************************************/

/* Stores internal component configuration for Unconfigured mode */
#if (spi_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Common configuration variables */
    uint8 spi_scbMode = spi_SCB_MODE_UNCONFIG;
    uint8 spi_scbEnableWake;
    uint8 spi_scbEnableIntr;

    /* I2C configuration variables */
    uint8 spi_mode;
    uint8 spi_acceptAddr;

    /* SPI/UART configuration variables */
    volatile uint8 * spi_rxBuffer;
    uint8  spi_rxDataBits;
    uint32 spi_rxBufferSize;

    volatile uint8 * spi_txBuffer;
    uint8  spi_txDataBits;
    uint32 spi_txBufferSize;

    /* EZI2C configuration variables */
    uint8 spi_numberOfAddr;
    uint8 spi_subAddrSize;
#endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */


/***************************************
*     Common SCB Vars
***************************************/
/**
* \addtogroup group_general
* \{
*/

/** spi_initVar indicates whether the spi 
*  component has been initialized. The variable is initialized to 0 
*  and set to 1 the first time SCB_Start() is called. This allows 
*  the component to restart without reinitialization after the first 
*  call to the spi_Start() routine.
*
*  If re-initialization of the component is required, then the 
*  spi_Init() function can be called before the 
*  spi_Start() or spi_Enable() function.
*/
uint8 spi_initVar = 0u;


#if (! (spi_SCB_MODE_I2C_CONST_CFG || \
        spi_SCB_MODE_EZI2C_CONST_CFG))
    /** This global variable stores TX interrupt sources after 
    * spi_Stop() is called. Only these TX interrupt sources 
    * will be restored on a subsequent spi_Enable() call.
    */
    uint16 spi_IntrTxMask = 0u;
#endif /* (! (spi_SCB_MODE_I2C_CONST_CFG || \
              spi_SCB_MODE_EZI2C_CONST_CFG)) */
/** \} globals */

#if (spi_SCB_IRQ_INTERNAL)
#if !defined (CY_REMOVE_spi_CUSTOM_INTR_HANDLER)
    void (*spi_customIntrHandler)(void) = NULL;
#endif /* !defined (CY_REMOVE_spi_CUSTOM_INTR_HANDLER) */
#endif /* (spi_SCB_IRQ_INTERNAL) */


/***************************************
*    Private Function Prototypes
***************************************/

static void spi_ScbEnableIntr(void);
static void spi_ScbModeStop(void);
static void spi_ScbModePostEnable(void);


/*******************************************************************************
* Function Name: spi_Init
****************************************************************************//**
*
*  Initializes the spi component to operate in one of the selected
*  configurations: I2C, SPI, UART or EZI2C.
*  When the configuration is set to "Unconfigured SCB", this function does
*  not do any initialization. Use mode-specific initialization APIs instead:
*  spi_I2CInit, spi_SpiInit, 
*  spi_UartInit or spi_EzI2CInit.
*
*******************************************************************************/
void spi_Init(void)
{
#if (spi_SCB_MODE_UNCONFIG_CONST_CFG)
    if (spi_SCB_MODE_UNCONFIG_RUNTM_CFG)
    {
        spi_initVar = 0u;
    }
    else
    {
        /* Initialization was done before this function call */
    }

#elif (spi_SCB_MODE_I2C_CONST_CFG)
    spi_I2CInit();

#elif (spi_SCB_MODE_SPI_CONST_CFG)
    spi_SpiInit();

#elif (spi_SCB_MODE_UART_CONST_CFG)
    spi_UartInit();

#elif (spi_SCB_MODE_EZI2C_CONST_CFG)
    spi_EzI2CInit();

#endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: spi_Enable
****************************************************************************//**
*
*  Enables spi component operation: activates the hardware and 
*  internal interrupt. It also restores TX interrupt sources disabled after the 
*  spi_Stop() function was called (note that level-triggered TX 
*  interrupt sources remain disabled to not cause code lock-up).
*  For I2C and EZI2C modes the interrupt is internal and mandatory for 
*  operation. For SPI and UART modes the interrupt can be configured as none, 
*  internal or external.
*  The spi configuration should be not changed when the component
*  is enabled. Any configuration changes should be made after disabling the 
*  component.
*  When configuration is set to “Unconfigured spi”, the component 
*  must first be initialized to operate in one of the following configurations: 
*  I2C, SPI, UART or EZ I2C, using the mode-specific initialization API. 
*  Otherwise this function does not enable the component.
*
*******************************************************************************/
void spi_Enable(void)
{
#if (spi_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Enable SCB block, only if it is already configured */
    if (!spi_SCB_MODE_UNCONFIG_RUNTM_CFG)
    {
        spi_CTRL_REG |= spi_CTRL_ENABLED;

        spi_ScbEnableIntr();

        /* Call PostEnable function specific to current operation mode */
        spi_ScbModePostEnable();
    }
#else
    spi_CTRL_REG |= spi_CTRL_ENABLED;

    spi_ScbEnableIntr();

    /* Call PostEnable function specific to current operation mode */
    spi_ScbModePostEnable();
#endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: spi_Start
****************************************************************************//**
*
*  Invokes spi_Init() and spi_Enable().
*  After this function call, the component is enabled and ready for operation.
*  When configuration is set to "Unconfigured SCB", the component must first be
*  initialized to operate in one of the following configurations: I2C, SPI, UART
*  or EZI2C. Otherwise this function does not enable the component.
*
* \globalvars
*  spi_initVar - used to check initial configuration, modified
*  on first function call.
*
*******************************************************************************/
void spi_Start(void)
{
    if (0u == spi_initVar)
    {
        spi_Init();
        spi_initVar = 1u; /* Component was initialized */
    }

    spi_Enable();
}


/*******************************************************************************
* Function Name: spi_Stop
****************************************************************************//**
*
*  Disables the spi component: disable the hardware and internal 
*  interrupt. It also disables all TX interrupt sources so as not to cause an 
*  unexpected interrupt trigger because after the component is enabled, the 
*  TX FIFO is empty.
*  Refer to the function spi_Enable() for the interrupt 
*  configuration details.
*  This function disables the SCB component without checking to see if 
*  communication is in progress. Before calling this function it may be 
*  necessary to check the status of communication to make sure communication 
*  is complete. If this is not done then communication could be stopped mid 
*  byte and corrupted data could result.
*
*******************************************************************************/
void spi_Stop(void)
{
#if (spi_SCB_IRQ_INTERNAL)
    spi_DisableInt();
#endif /* (spi_SCB_IRQ_INTERNAL) */

    /* Call Stop function specific to current operation mode */
    spi_ScbModeStop();

    /* Disable SCB IP */
    spi_CTRL_REG &= (uint32) ~spi_CTRL_ENABLED;

    /* Disable all TX interrupt sources so as not to cause an unexpected
    * interrupt trigger after the component will be enabled because the 
    * TX FIFO is empty.
    * For SCB IP v0, it is critical as it does not mask-out interrupt
    * sources when it is disabled. This can cause a code lock-up in the
    * interrupt handler because TX FIFO cannot be loaded after the block
    * is disabled.
    */
    spi_SetTxInterruptMode(spi_NO_INTR_SOURCES);

#if (spi_SCB_IRQ_INTERNAL)
    spi_ClearPendingInt();
#endif /* (spi_SCB_IRQ_INTERNAL) */
}


/*******************************************************************************
* Function Name: spi_SetRxFifoLevel
****************************************************************************//**
*
*  Sets level in the RX FIFO to generate a RX level interrupt.
*  When the RX FIFO has more entries than the RX FIFO level an RX level
*  interrupt request is generated.
*
*  \param level: Level in the RX FIFO to generate RX level interrupt.
*   The range of valid level values is between 0 and RX FIFO depth - 1.
*
*******************************************************************************/
void spi_SetRxFifoLevel(uint32 level)
{
    uint32 rxFifoCtrl;

    rxFifoCtrl = spi_RX_FIFO_CTRL_REG;

    rxFifoCtrl &= ((uint32) ~spi_RX_FIFO_CTRL_TRIGGER_LEVEL_MASK); /* Clear level mask bits */
    rxFifoCtrl |= ((uint32) (spi_RX_FIFO_CTRL_TRIGGER_LEVEL_MASK & level));

    spi_RX_FIFO_CTRL_REG = rxFifoCtrl;
}


/*******************************************************************************
* Function Name: spi_SetTxFifoLevel
****************************************************************************//**
*
*  Sets level in the TX FIFO to generate a TX level interrupt.
*  When the TX FIFO has less entries than the TX FIFO level an TX level
*  interrupt request is generated.
*
*  \param level: Level in the TX FIFO to generate TX level interrupt.
*   The range of valid level values is between 0 and TX FIFO depth - 1.
*
*******************************************************************************/
void spi_SetTxFifoLevel(uint32 level)
{
    uint32 txFifoCtrl;

    txFifoCtrl = spi_TX_FIFO_CTRL_REG;

    txFifoCtrl &= ((uint32) ~spi_TX_FIFO_CTRL_TRIGGER_LEVEL_MASK); /* Clear level mask bits */
    txFifoCtrl |= ((uint32) (spi_TX_FIFO_CTRL_TRIGGER_LEVEL_MASK & level));

    spi_TX_FIFO_CTRL_REG = txFifoCtrl;
}


#if (spi_SCB_IRQ_INTERNAL)
    /*******************************************************************************
    * Function Name: spi_SetCustomInterruptHandler
    ****************************************************************************//**
    *
    *  Registers a function to be called by the internal interrupt handler.
    *  First the function that is registered is called, then the internal interrupt
    *  handler performs any operation such as software buffer management functions
    *  before the interrupt returns.  It is the user's responsibility not to break
    *  the software buffer operations. Only one custom handler is supported, which
    *  is the function provided by the most recent call.
    *  At the initialization time no custom handler is registered.
    *
    *  \param func: Pointer to the function to register.
    *        The value NULL indicates to remove the current custom interrupt
    *        handler.
    *
    *******************************************************************************/
    void spi_SetCustomInterruptHandler(void (*func)(void))
    {
    #if !defined (CY_REMOVE_spi_CUSTOM_INTR_HANDLER)
        spi_customIntrHandler = func; /* Register interrupt handler */
    #else
        if (NULL != func)
        {
            /* Suppress compiler warning */
        }
    #endif /* !defined (CY_REMOVE_spi_CUSTOM_INTR_HANDLER) */
    }
#endif /* (spi_SCB_IRQ_INTERNAL) */


/*******************************************************************************
* Function Name: spi_ScbModeEnableIntr
****************************************************************************//**
*
*  Enables an interrupt for a specific mode.
*
*******************************************************************************/
static void spi_ScbEnableIntr(void)
{
#if (spi_SCB_IRQ_INTERNAL)
    /* Enable interrupt in NVIC */
    #if (spi_SCB_MODE_UNCONFIG_CONST_CFG)
        if (0u != spi_scbEnableIntr)
        {
            spi_EnableInt();
        }

    #else
        spi_EnableInt();

    #endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */
#endif /* (spi_SCB_IRQ_INTERNAL) */
}


/*******************************************************************************
* Function Name: spi_ScbModePostEnable
****************************************************************************//**
*
*  Calls the PostEnable function for a specific operation mode.
*
*******************************************************************************/
static void spi_ScbModePostEnable(void)
{
#if (spi_SCB_MODE_UNCONFIG_CONST_CFG)
#if (!spi_CY_SCBIP_V1)
    if (spi_SCB_MODE_SPI_RUNTM_CFG)
    {
        spi_SpiPostEnable();
    }
    else if (spi_SCB_MODE_UART_RUNTM_CFG)
    {
        spi_UartPostEnable();
    }
    else
    {
        /* Unknown mode: do nothing */
    }
#endif /* (!spi_CY_SCBIP_V1) */

#elif (spi_SCB_MODE_SPI_CONST_CFG)
    spi_SpiPostEnable();

#elif (spi_SCB_MODE_UART_CONST_CFG)
    spi_UartPostEnable();

#else
    /* Unknown mode: do nothing */
#endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: spi_ScbModeStop
****************************************************************************//**
*
*  Calls the Stop function for a specific operation mode.
*
*******************************************************************************/
static void spi_ScbModeStop(void)
{
#if (spi_SCB_MODE_UNCONFIG_CONST_CFG)
    if (spi_SCB_MODE_I2C_RUNTM_CFG)
    {
        spi_I2CStop();
    }
    else if (spi_SCB_MODE_EZI2C_RUNTM_CFG)
    {
        spi_EzI2CStop();
    }
#if (!spi_CY_SCBIP_V1)
    else if (spi_SCB_MODE_SPI_RUNTM_CFG)
    {
        spi_SpiStop();
    }
    else if (spi_SCB_MODE_UART_RUNTM_CFG)
    {
        spi_UartStop();
    }
#endif /* (!spi_CY_SCBIP_V1) */
    else
    {
        /* Unknown mode: do nothing */
    }
#elif (spi_SCB_MODE_I2C_CONST_CFG)
    spi_I2CStop();

#elif (spi_SCB_MODE_EZI2C_CONST_CFG)
    spi_EzI2CStop();

#elif (spi_SCB_MODE_SPI_CONST_CFG)
    spi_SpiStop();

#elif (spi_SCB_MODE_UART_CONST_CFG)
    spi_UartStop();

#else
    /* Unknown mode: do nothing */
#endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */
}


#if (spi_SCB_MODE_UNCONFIG_CONST_CFG)
    /*******************************************************************************
    * Function Name: spi_SetPins
    ****************************************************************************//**
    *
    *  Sets the pins settings accordingly to the selected operation mode.
    *  Only available in the Unconfigured operation mode. The mode specific
    *  initialization function calls it.
    *  Pins configuration is set by PSoC Creator when a specific mode of operation
    *  is selected in design time.
    *
    *  \param mode:      Mode of SCB operation.
    *  \param subMode:   Sub-mode of SCB operation. It is only required for SPI and UART
    *             modes.
    *  \param uartEnableMask: enables TX or RX direction and RTS and CTS signals.
    *
    *******************************************************************************/
    void spi_SetPins(uint32 mode, uint32 subMode, uint32 uartEnableMask)
    {
        uint32 pinsDm[spi_SCB_PINS_NUMBER];
        uint32 i;
        
    #if (!spi_CY_SCBIP_V1)
        uint32 pinsInBuf = 0u;
    #endif /* (!spi_CY_SCBIP_V1) */
        
        uint32 hsiomSel[spi_SCB_PINS_NUMBER] = 
        {
            spi_RX_SDA_MOSI_HSIOM_SEL_GPIO,
            spi_TX_SCL_MISO_HSIOM_SEL_GPIO,
            0u,
            0u,
            0u,
            0u,
            0u,
        };

    #if (spi_CY_SCBIP_V1)
        /* Supress compiler warning. */
        if ((0u == subMode) || (0u == uartEnableMask))
        {
        }
    #endif /* (spi_CY_SCBIP_V1) */

        /* Set default HSIOM to GPIO and Drive Mode to Analog Hi-Z */
        for (i = 0u; i < spi_SCB_PINS_NUMBER; i++)
        {
            pinsDm[i] = spi_PIN_DM_ALG_HIZ;
        }

        if ((spi_SCB_MODE_I2C   == mode) ||
            (spi_SCB_MODE_EZI2C == mode))
        {
        #if (spi_RX_SDA_MOSI_PIN)
            hsiomSel[spi_RX_SDA_MOSI_PIN_INDEX] = spi_RX_SDA_MOSI_HSIOM_SEL_I2C;
            pinsDm  [spi_RX_SDA_MOSI_PIN_INDEX] = spi_PIN_DM_OD_LO;
        #elif (spi_RX_WAKE_SDA_MOSI_PIN)
            hsiomSel[spi_RX_WAKE_SDA_MOSI_PIN_INDEX] = spi_RX_WAKE_SDA_MOSI_HSIOM_SEL_I2C;
            pinsDm  [spi_RX_WAKE_SDA_MOSI_PIN_INDEX] = spi_PIN_DM_OD_LO;
        #else
        #endif /* (spi_RX_SDA_MOSI_PIN) */
        
        #if (spi_TX_SCL_MISO_PIN)
            hsiomSel[spi_TX_SCL_MISO_PIN_INDEX] = spi_TX_SCL_MISO_HSIOM_SEL_I2C;
            pinsDm  [spi_TX_SCL_MISO_PIN_INDEX] = spi_PIN_DM_OD_LO;
        #endif /* (spi_TX_SCL_MISO_PIN) */
        }
    #if (!spi_CY_SCBIP_V1)
        else if (spi_SCB_MODE_SPI == mode)
        {
        #if (spi_RX_SDA_MOSI_PIN)
            hsiomSel[spi_RX_SDA_MOSI_PIN_INDEX] = spi_RX_SDA_MOSI_HSIOM_SEL_SPI;
        #elif (spi_RX_WAKE_SDA_MOSI_PIN)
            hsiomSel[spi_RX_WAKE_SDA_MOSI_PIN_INDEX] = spi_RX_WAKE_SDA_MOSI_HSIOM_SEL_SPI;
        #else
        #endif /* (spi_RX_SDA_MOSI_PIN) */
        
        #if (spi_TX_SCL_MISO_PIN)
            hsiomSel[spi_TX_SCL_MISO_PIN_INDEX] = spi_TX_SCL_MISO_HSIOM_SEL_SPI;
        #endif /* (spi_TX_SCL_MISO_PIN) */
        
        #if (spi_CTS_SCLK_PIN)
            hsiomSel[spi_CTS_SCLK_PIN_INDEX] = spi_CTS_SCLK_HSIOM_SEL_SPI;
        #endif /* (spi_CTS_SCLK_PIN) */

            if (spi_SPI_SLAVE == subMode)
            {
                /* Slave */
                pinsDm[spi_RX_SDA_MOSI_PIN_INDEX] = spi_PIN_DM_DIG_HIZ;
                pinsDm[spi_TX_SCL_MISO_PIN_INDEX] = spi_PIN_DM_STRONG;
                pinsDm[spi_CTS_SCLK_PIN_INDEX] = spi_PIN_DM_DIG_HIZ;

            #if (spi_RTS_SS0_PIN)
                /* Only SS0 is valid choice for Slave */
                hsiomSel[spi_RTS_SS0_PIN_INDEX] = spi_RTS_SS0_HSIOM_SEL_SPI;
                pinsDm  [spi_RTS_SS0_PIN_INDEX] = spi_PIN_DM_DIG_HIZ;
            #endif /* (spi_RTS_SS0_PIN) */

            #if (spi_TX_SCL_MISO_PIN)
                /* Disable input buffer */
                 pinsInBuf |= spi_TX_SCL_MISO_PIN_MASK;
            #endif /* (spi_TX_SCL_MISO_PIN) */
            }
            else 
            {
                /* (Master) */
                pinsDm[spi_RX_SDA_MOSI_PIN_INDEX] = spi_PIN_DM_STRONG;
                pinsDm[spi_TX_SCL_MISO_PIN_INDEX] = spi_PIN_DM_DIG_HIZ;
                pinsDm[spi_CTS_SCLK_PIN_INDEX] = spi_PIN_DM_STRONG;

            #if (spi_RTS_SS0_PIN)
                hsiomSel [spi_RTS_SS0_PIN_INDEX] = spi_RTS_SS0_HSIOM_SEL_SPI;
                pinsDm   [spi_RTS_SS0_PIN_INDEX] = spi_PIN_DM_STRONG;
                pinsInBuf |= spi_RTS_SS0_PIN_MASK;
            #endif /* (spi_RTS_SS0_PIN) */

            #if (spi_SS1_PIN)
                hsiomSel [spi_SS1_PIN_INDEX] = spi_SS1_HSIOM_SEL_SPI;
                pinsDm   [spi_SS1_PIN_INDEX] = spi_PIN_DM_STRONG;
                pinsInBuf |= spi_SS1_PIN_MASK;
            #endif /* (spi_SS1_PIN) */

            #if (spi_SS2_PIN)
                hsiomSel [spi_SS2_PIN_INDEX] = spi_SS2_HSIOM_SEL_SPI;
                pinsDm   [spi_SS2_PIN_INDEX] = spi_PIN_DM_STRONG;
                pinsInBuf |= spi_SS2_PIN_MASK;
            #endif /* (spi_SS2_PIN) */

            #if (spi_SS3_PIN)
                hsiomSel [spi_SS3_PIN_INDEX] = spi_SS3_HSIOM_SEL_SPI;
                pinsDm   [spi_SS3_PIN_INDEX] = spi_PIN_DM_STRONG;
                pinsInBuf |= spi_SS3_PIN_MASK;
            #endif /* (spi_SS3_PIN) */

                /* Disable input buffers */
            #if (spi_RX_SDA_MOSI_PIN)
                pinsInBuf |= spi_RX_SDA_MOSI_PIN_MASK;
            #elif (spi_RX_WAKE_SDA_MOSI_PIN)
                pinsInBuf |= spi_RX_WAKE_SDA_MOSI_PIN_MASK;
            #else
            #endif /* (spi_RX_SDA_MOSI_PIN) */

            #if (spi_CTS_SCLK_PIN)
                pinsInBuf |= spi_CTS_SCLK_PIN_MASK;
            #endif /* (spi_CTS_SCLK_PIN) */
            }
        }
        else /* UART */
        {
            if (spi_UART_MODE_SMARTCARD == subMode)
            {
                /* SmartCard */
            #if (spi_TX_SCL_MISO_PIN)
                hsiomSel[spi_TX_SCL_MISO_PIN_INDEX] = spi_TX_SCL_MISO_HSIOM_SEL_UART;
                pinsDm  [spi_TX_SCL_MISO_PIN_INDEX] = spi_PIN_DM_OD_LO;
            #endif /* (spi_TX_SCL_MISO_PIN) */
            }
            else /* Standard or IrDA */
            {
                if (0u != (spi_UART_RX_PIN_ENABLE & uartEnableMask))
                {
                #if (spi_RX_SDA_MOSI_PIN)
                    hsiomSel[spi_RX_SDA_MOSI_PIN_INDEX] = spi_RX_SDA_MOSI_HSIOM_SEL_UART;
                    pinsDm  [spi_RX_SDA_MOSI_PIN_INDEX] = spi_PIN_DM_DIG_HIZ;
                #elif (spi_RX_WAKE_SDA_MOSI_PIN)
                    hsiomSel[spi_RX_WAKE_SDA_MOSI_PIN_INDEX] = spi_RX_WAKE_SDA_MOSI_HSIOM_SEL_UART;
                    pinsDm  [spi_RX_WAKE_SDA_MOSI_PIN_INDEX] = spi_PIN_DM_DIG_HIZ;
                #else
                #endif /* (spi_RX_SDA_MOSI_PIN) */
                }

                if (0u != (spi_UART_TX_PIN_ENABLE & uartEnableMask))
                {
                #if (spi_TX_SCL_MISO_PIN)
                    hsiomSel[spi_TX_SCL_MISO_PIN_INDEX] = spi_TX_SCL_MISO_HSIOM_SEL_UART;
                    pinsDm  [spi_TX_SCL_MISO_PIN_INDEX] = spi_PIN_DM_STRONG;
                    
                    /* Disable input buffer */
                    pinsInBuf |= spi_TX_SCL_MISO_PIN_MASK;
                #endif /* (spi_TX_SCL_MISO_PIN) */
                }

            #if !(spi_CY_SCBIP_V0 || spi_CY_SCBIP_V1)
                if (spi_UART_MODE_STD == subMode)
                {
                    if (0u != (spi_UART_CTS_PIN_ENABLE & uartEnableMask))
                    {
                        /* CTS input is multiplexed with SCLK */
                    #if (spi_CTS_SCLK_PIN)
                        hsiomSel[spi_CTS_SCLK_PIN_INDEX] = spi_CTS_SCLK_HSIOM_SEL_UART;
                        pinsDm  [spi_CTS_SCLK_PIN_INDEX] = spi_PIN_DM_DIG_HIZ;
                    #endif /* (spi_CTS_SCLK_PIN) */
                    }

                    if (0u != (spi_UART_RTS_PIN_ENABLE & uartEnableMask))
                    {
                        /* RTS output is multiplexed with SS0 */
                    #if (spi_RTS_SS0_PIN)
                        hsiomSel[spi_RTS_SS0_PIN_INDEX] = spi_RTS_SS0_HSIOM_SEL_UART;
                        pinsDm  [spi_RTS_SS0_PIN_INDEX] = spi_PIN_DM_STRONG;
                        
                        /* Disable input buffer */
                        pinsInBuf |= spi_RTS_SS0_PIN_MASK;
                    #endif /* (spi_RTS_SS0_PIN) */
                    }
                }
            #endif /* !(spi_CY_SCBIP_V0 || spi_CY_SCBIP_V1) */
            }
        }
    #endif /* (!spi_CY_SCBIP_V1) */

    /* Configure pins: set HSIOM, DM and InputBufEnable */
    /* Note: the DR register settings do not effect the pin output if HSIOM is other than GPIO */

    #if (spi_RX_SDA_MOSI_PIN)
        spi_SET_HSIOM_SEL(spi_RX_SDA_MOSI_HSIOM_REG,
                                       spi_RX_SDA_MOSI_HSIOM_MASK,
                                       spi_RX_SDA_MOSI_HSIOM_POS,
                                        hsiomSel[spi_RX_SDA_MOSI_PIN_INDEX]);

        spi_uart_rx_i2c_sda_spi_mosi_SetDriveMode((uint8) pinsDm[spi_RX_SDA_MOSI_PIN_INDEX]);

        #if (!spi_CY_SCBIP_V1)
            spi_SET_INP_DIS(spi_uart_rx_i2c_sda_spi_mosi_INP_DIS,
                                         spi_uart_rx_i2c_sda_spi_mosi_MASK,
                                         (0u != (pinsInBuf & spi_RX_SDA_MOSI_PIN_MASK)));
        #endif /* (!spi_CY_SCBIP_V1) */
    
    #elif (spi_RX_WAKE_SDA_MOSI_PIN)
        spi_SET_HSIOM_SEL(spi_RX_WAKE_SDA_MOSI_HSIOM_REG,
                                       spi_RX_WAKE_SDA_MOSI_HSIOM_MASK,
                                       spi_RX_WAKE_SDA_MOSI_HSIOM_POS,
                                       hsiomSel[spi_RX_WAKE_SDA_MOSI_PIN_INDEX]);

        spi_uart_rx_wake_i2c_sda_spi_mosi_SetDriveMode((uint8)
                                                               pinsDm[spi_RX_WAKE_SDA_MOSI_PIN_INDEX]);

        spi_SET_INP_DIS(spi_uart_rx_wake_i2c_sda_spi_mosi_INP_DIS,
                                     spi_uart_rx_wake_i2c_sda_spi_mosi_MASK,
                                     (0u != (pinsInBuf & spi_RX_WAKE_SDA_MOSI_PIN_MASK)));

         /* Set interrupt on falling edge */
        spi_SET_INCFG_TYPE(spi_RX_WAKE_SDA_MOSI_INTCFG_REG,
                                        spi_RX_WAKE_SDA_MOSI_INTCFG_TYPE_MASK,
                                        spi_RX_WAKE_SDA_MOSI_INTCFG_TYPE_POS,
                                        spi_INTCFG_TYPE_FALLING_EDGE);
    #else
    #endif /* (spi_RX_WAKE_SDA_MOSI_PIN) */

    #if (spi_TX_SCL_MISO_PIN)
        spi_SET_HSIOM_SEL(spi_TX_SCL_MISO_HSIOM_REG,
                                       spi_TX_SCL_MISO_HSIOM_MASK,
                                       spi_TX_SCL_MISO_HSIOM_POS,
                                        hsiomSel[spi_TX_SCL_MISO_PIN_INDEX]);

        spi_uart_tx_i2c_scl_spi_miso_SetDriveMode((uint8) pinsDm[spi_TX_SCL_MISO_PIN_INDEX]);

    #if (!spi_CY_SCBIP_V1)
        spi_SET_INP_DIS(spi_uart_tx_i2c_scl_spi_miso_INP_DIS,
                                     spi_uart_tx_i2c_scl_spi_miso_MASK,
                                    (0u != (pinsInBuf & spi_TX_SCL_MISO_PIN_MASK)));
    #endif /* (!spi_CY_SCBIP_V1) */
    #endif /* (spi_RX_SDA_MOSI_PIN) */

    #if (spi_CTS_SCLK_PIN)
        spi_SET_HSIOM_SEL(spi_CTS_SCLK_HSIOM_REG,
                                       spi_CTS_SCLK_HSIOM_MASK,
                                       spi_CTS_SCLK_HSIOM_POS,
                                       hsiomSel[spi_CTS_SCLK_PIN_INDEX]);

        spi_uart_cts_spi_sclk_SetDriveMode((uint8) pinsDm[spi_CTS_SCLK_PIN_INDEX]);

        spi_SET_INP_DIS(spi_uart_cts_spi_sclk_INP_DIS,
                                     spi_uart_cts_spi_sclk_MASK,
                                     (0u != (pinsInBuf & spi_CTS_SCLK_PIN_MASK)));
    #endif /* (spi_CTS_SCLK_PIN) */

    #if (spi_RTS_SS0_PIN)
        spi_SET_HSIOM_SEL(spi_RTS_SS0_HSIOM_REG,
                                       spi_RTS_SS0_HSIOM_MASK,
                                       spi_RTS_SS0_HSIOM_POS,
                                       hsiomSel[spi_RTS_SS0_PIN_INDEX]);

        spi_uart_rts_spi_ss0_SetDriveMode((uint8) pinsDm[spi_RTS_SS0_PIN_INDEX]);

        spi_SET_INP_DIS(spi_uart_rts_spi_ss0_INP_DIS,
                                     spi_uart_rts_spi_ss0_MASK,
                                     (0u != (pinsInBuf & spi_RTS_SS0_PIN_MASK)));
    #endif /* (spi_RTS_SS0_PIN) */

    #if (spi_SS1_PIN)
        spi_SET_HSIOM_SEL(spi_SS1_HSIOM_REG,
                                       spi_SS1_HSIOM_MASK,
                                       spi_SS1_HSIOM_POS,
                                       hsiomSel[spi_SS1_PIN_INDEX]);

        spi_spi_ss1_SetDriveMode((uint8) pinsDm[spi_SS1_PIN_INDEX]);

        spi_SET_INP_DIS(spi_spi_ss1_INP_DIS,
                                     spi_spi_ss1_MASK,
                                     (0u != (pinsInBuf & spi_SS1_PIN_MASK)));
    #endif /* (spi_SS1_PIN) */

    #if (spi_SS2_PIN)
        spi_SET_HSIOM_SEL(spi_SS2_HSIOM_REG,
                                       spi_SS2_HSIOM_MASK,
                                       spi_SS2_HSIOM_POS,
                                       hsiomSel[spi_SS2_PIN_INDEX]);

        spi_spi_ss2_SetDriveMode((uint8) pinsDm[spi_SS2_PIN_INDEX]);

        spi_SET_INP_DIS(spi_spi_ss2_INP_DIS,
                                     spi_spi_ss2_MASK,
                                     (0u != (pinsInBuf & spi_SS2_PIN_MASK)));
    #endif /* (spi_SS2_PIN) */

    #if (spi_SS3_PIN)
        spi_SET_HSIOM_SEL(spi_SS3_HSIOM_REG,
                                       spi_SS3_HSIOM_MASK,
                                       spi_SS3_HSIOM_POS,
                                       hsiomSel[spi_SS3_PIN_INDEX]);

        spi_spi_ss3_SetDriveMode((uint8) pinsDm[spi_SS3_PIN_INDEX]);

        spi_SET_INP_DIS(spi_spi_ss3_INP_DIS,
                                     spi_spi_ss3_MASK,
                                     (0u != (pinsInBuf & spi_SS3_PIN_MASK)));
    #endif /* (spi_SS3_PIN) */
    }

#endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */


#if (spi_CY_SCBIP_V0 || spi_CY_SCBIP_V1)
    /*******************************************************************************
    * Function Name: spi_I2CSlaveNackGeneration
    ****************************************************************************//**
    *
    *  Sets command to generate NACK to the address or data.
    *
    *******************************************************************************/
    void spi_I2CSlaveNackGeneration(void)
    {
        /* Check for EC_AM toggle condition: EC_AM and clock stretching for address are enabled */
        if ((0u != (spi_CTRL_REG & spi_CTRL_EC_AM_MODE)) &&
            (0u == (spi_I2C_CTRL_REG & spi_I2C_CTRL_S_NOT_READY_ADDR_NACK)))
        {
            /* Toggle EC_AM before NACK generation */
            spi_CTRL_REG &= ~spi_CTRL_EC_AM_MODE;
            spi_CTRL_REG |=  spi_CTRL_EC_AM_MODE;
        }

        spi_I2C_SLAVE_CMD_REG = spi_I2C_SLAVE_CMD_S_NACK;
    }
#endif /* (spi_CY_SCBIP_V0 || spi_CY_SCBIP_V1) */


/* [] END OF FILE */
