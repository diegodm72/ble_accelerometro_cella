/***************************************************************************//**
* \file scb_BOOT.h
* \version 4.0
*
* \brief
*  This file provides constants and parameter values of the bootloader
*  communication APIs for the SCB Component.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2014-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_BOOT_scb_H)
#define CY_SCB_BOOT_scb_H

#include "scb_PVT.h"

#if (scb_SCB_MODE_I2C_INC)
    #include "scb_I2C.h"
#endif /* (scb_SCB_MODE_I2C_INC) */

#if (scb_SCB_MODE_EZI2C_INC)
    #include "scb_EZI2C.h"
#endif /* (scb_SCB_MODE_EZI2C_INC) */

#if (scb_SCB_MODE_SPI_INC || scb_SCB_MODE_UART_INC)
    #include "scb_SPI_UART.h"
#endif /* (scb_SCB_MODE_SPI_INC || scb_SCB_MODE_UART_INC) */


/***************************************
*  Conditional Compilation Parameters
****************************************/

/* Bootloader communication interface enable */
#define scb_BTLDR_COMM_ENABLED ((CYDEV_BOOTLOADER_IO_COMP == CyBtldr_scb) || \
                                             (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_Custom_Interface))

/* Enable I2C bootloader communication */
#if (scb_SCB_MODE_I2C_INC)
    #define scb_I2C_BTLDR_COMM_ENABLED     (scb_BTLDR_COMM_ENABLED && \
                                                            (scb_SCB_MODE_UNCONFIG_CONST_CFG || \
                                                             scb_I2C_SLAVE_CONST))
#else
     #define scb_I2C_BTLDR_COMM_ENABLED    (0u)
#endif /* (scb_SCB_MODE_I2C_INC) */

/* EZI2C does not support bootloader communication. Provide empty APIs */
#if (scb_SCB_MODE_EZI2C_INC)
    #define scb_EZI2C_BTLDR_COMM_ENABLED   (scb_BTLDR_COMM_ENABLED && \
                                                         scb_SCB_MODE_UNCONFIG_CONST_CFG)
#else
    #define scb_EZI2C_BTLDR_COMM_ENABLED   (0u)
#endif /* (scb_EZI2C_BTLDR_COMM_ENABLED) */

/* Enable SPI bootloader communication */
#if (scb_SCB_MODE_SPI_INC)
    #define scb_SPI_BTLDR_COMM_ENABLED     (scb_BTLDR_COMM_ENABLED && \
                                                            (scb_SCB_MODE_UNCONFIG_CONST_CFG || \
                                                             scb_SPI_SLAVE_CONST))
#else
        #define scb_SPI_BTLDR_COMM_ENABLED (0u)
#endif /* (scb_SPI_BTLDR_COMM_ENABLED) */

/* Enable UART bootloader communication */
#if (scb_SCB_MODE_UART_INC)
       #define scb_UART_BTLDR_COMM_ENABLED    (scb_BTLDR_COMM_ENABLED && \
                                                            (scb_SCB_MODE_UNCONFIG_CONST_CFG || \
                                                             (scb_UART_RX_DIRECTION && \
                                                              scb_UART_TX_DIRECTION)))
#else
     #define scb_UART_BTLDR_COMM_ENABLED   (0u)
#endif /* (scb_UART_BTLDR_COMM_ENABLED) */

/* Enable bootloader communication */
#define scb_BTLDR_COMM_MODE_ENABLED    (scb_I2C_BTLDR_COMM_ENABLED   || \
                                                     scb_SPI_BTLDR_COMM_ENABLED   || \
                                                     scb_EZI2C_BTLDR_COMM_ENABLED || \
                                                     scb_UART_BTLDR_COMM_ENABLED)


/***************************************
*        Function Prototypes
***************************************/

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (scb_I2C_BTLDR_COMM_ENABLED)
    /* I2C Bootloader physical layer functions */
    void scb_I2CCyBtldrCommStart(void);
    void scb_I2CCyBtldrCommStop (void);
    void scb_I2CCyBtldrCommReset(void);
    cystatus scb_I2CCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus scb_I2CCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map I2C specific bootloader communication APIs to SCB specific APIs */
    #if (scb_SCB_MODE_I2C_CONST_CFG)
        #define scb_CyBtldrCommStart   scb_I2CCyBtldrCommStart
        #define scb_CyBtldrCommStop    scb_I2CCyBtldrCommStop
        #define scb_CyBtldrCommReset   scb_I2CCyBtldrCommReset
        #define scb_CyBtldrCommRead    scb_I2CCyBtldrCommRead
        #define scb_CyBtldrCommWrite   scb_I2CCyBtldrCommWrite
    #endif /* (scb_SCB_MODE_I2C_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (scb_I2C_BTLDR_COMM_ENABLED) */


#if defined(CYDEV_BOOTLOADER_IO_COMP) && (scb_EZI2C_BTLDR_COMM_ENABLED)
    /* Bootloader physical layer functions */
    void scb_EzI2CCyBtldrCommStart(void);
    void scb_EzI2CCyBtldrCommStop (void);
    void scb_EzI2CCyBtldrCommReset(void);
    cystatus scb_EzI2CCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus scb_EzI2CCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map EZI2C specific bootloader communication APIs to SCB specific APIs */
    #if (scb_SCB_MODE_EZI2C_CONST_CFG)
        #define scb_CyBtldrCommStart   scb_EzI2CCyBtldrCommStart
        #define scb_CyBtldrCommStop    scb_EzI2CCyBtldrCommStop
        #define scb_CyBtldrCommReset   scb_EzI2CCyBtldrCommReset
        #define scb_CyBtldrCommRead    scb_EzI2CCyBtldrCommRead
        #define scb_CyBtldrCommWrite   scb_EzI2CCyBtldrCommWrite
    #endif /* (scb_SCB_MODE_EZI2C_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (scb_EZI2C_BTLDR_COMM_ENABLED) */

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (scb_SPI_BTLDR_COMM_ENABLED)
    /* SPI Bootloader physical layer functions */
    void scb_SpiCyBtldrCommStart(void);
    void scb_SpiCyBtldrCommStop (void);
    void scb_SpiCyBtldrCommReset(void);
    cystatus scb_SpiCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus scb_SpiCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map SPI specific bootloader communication APIs to SCB specific APIs */
    #if (scb_SCB_MODE_SPI_CONST_CFG)
        #define scb_CyBtldrCommStart   scb_SpiCyBtldrCommStart
        #define scb_CyBtldrCommStop    scb_SpiCyBtldrCommStop
        #define scb_CyBtldrCommReset   scb_SpiCyBtldrCommReset
        #define scb_CyBtldrCommRead    scb_SpiCyBtldrCommRead
        #define scb_CyBtldrCommWrite   scb_SpiCyBtldrCommWrite
    #endif /* (scb_SCB_MODE_SPI_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (scb_SPI_BTLDR_COMM_ENABLED) */

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (scb_UART_BTLDR_COMM_ENABLED)
    /* UART Bootloader physical layer functions */
    void scb_UartCyBtldrCommStart(void);
    void scb_UartCyBtldrCommStop (void);
    void scb_UartCyBtldrCommReset(void);
    cystatus scb_UartCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus scb_UartCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map UART specific bootloader communication APIs to SCB specific APIs */
    #if (scb_SCB_MODE_UART_CONST_CFG)
        #define scb_CyBtldrCommStart   scb_UartCyBtldrCommStart
        #define scb_CyBtldrCommStop    scb_UartCyBtldrCommStop
        #define scb_CyBtldrCommReset   scb_UartCyBtldrCommReset
        #define scb_CyBtldrCommRead    scb_UartCyBtldrCommRead
        #define scb_CyBtldrCommWrite   scb_UartCyBtldrCommWrite
    #endif /* (scb_SCB_MODE_UART_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (scb_UART_BTLDR_COMM_ENABLED) */

/**
* \addtogroup group_bootloader
* @{
*/

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (scb_BTLDR_COMM_ENABLED)
    #if (scb_SCB_MODE_UNCONFIG_CONST_CFG)
        /* Bootloader physical layer functions */
        void scb_CyBtldrCommStart(void);
        void scb_CyBtldrCommStop (void);
        void scb_CyBtldrCommReset(void);
        cystatus scb_CyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
        cystatus scb_CyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    #endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */

    /* Map SCB specific bootloader communication APIs to common APIs */
    #if (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_scb)
        #define CyBtldrCommStart    scb_CyBtldrCommStart
        #define CyBtldrCommStop     scb_CyBtldrCommStop
        #define CyBtldrCommReset    scb_CyBtldrCommReset
        #define CyBtldrCommWrite    scb_CyBtldrCommWrite
        #define CyBtldrCommRead     scb_CyBtldrCommRead
    #endif /* (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_scb) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (scb_BTLDR_COMM_ENABLED) */

/** @} group_bootloader */

/***************************************
*           API Constants
***************************************/

/* Timeout unit in milliseconds */
#define scb_WAIT_1_MS  (1u)

/* Return number of bytes to copy into bootloader buffer */
#define scb_BYTES_TO_COPY(actBufSize, bufSize) \
                            ( ((uint32)(actBufSize) < (uint32)(bufSize)) ? \
                                ((uint32) (actBufSize)) : ((uint32) (bufSize)) )

/* Size of Read/Write buffers for I2C bootloader  */
#define scb_I2C_BTLDR_SIZEOF_READ_BUFFER   (64u)
#define scb_I2C_BTLDR_SIZEOF_WRITE_BUFFER  (64u)

/* Byte to byte time interval: calculated basing on current component
* data rate configuration, can be defined in project if required.
*/
#ifndef scb_SPI_BYTE_TO_BYTE
    #define scb_SPI_BYTE_TO_BYTE   (16u)
#endif

/* Byte to byte time interval: calculated basing on current component
* baud rate configuration, can be defined in the project if required.
*/
#ifndef scb_UART_BYTE_TO_BYTE
    #define scb_UART_BYTE_TO_BYTE  (2500u)
#endif /* scb_UART_BYTE_TO_BYTE */

#endif /* (CY_SCB_BOOT_scb_H) */


/* [] END OF FILE */
