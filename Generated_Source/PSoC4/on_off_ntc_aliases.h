/*******************************************************************************
* File Name: on_off_ntc.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_on_off_ntc_ALIASES_H) /* Pins on_off_ntc_ALIASES_H */
#define CY_PINS_on_off_ntc_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define on_off_ntc_0			(on_off_ntc__0__PC)
#define on_off_ntc_0_PS		(on_off_ntc__0__PS)
#define on_off_ntc_0_PC		(on_off_ntc__0__PC)
#define on_off_ntc_0_DR		(on_off_ntc__0__DR)
#define on_off_ntc_0_SHIFT	(on_off_ntc__0__SHIFT)
#define on_off_ntc_0_INTR	((uint16)((uint16)0x0003u << (on_off_ntc__0__SHIFT*2u)))

#define on_off_ntc_INTR_ALL	 ((uint16)(on_off_ntc_0_INTR))


#endif /* End Pins on_off_ntc_ALIASES_H */


/* [] END OF FILE */
