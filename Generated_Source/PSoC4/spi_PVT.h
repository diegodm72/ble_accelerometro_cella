/***************************************************************************//**
* \file .h
* \version 4.0
*
* \brief
*  This private file provides constants and parameter values for the
*  SCB Component.
*  Please do not use this file or its content in your project.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_PVT_spi_H)
#define CY_SCB_PVT_spi_H

#include "spi.h"


/***************************************
*     Private Function Prototypes
***************************************/

/* APIs to service INTR_I2C_EC register */
#define spi_SetI2CExtClkInterruptMode(interruptMask) spi_WRITE_INTR_I2C_EC_MASK(interruptMask)
#define spi_ClearI2CExtClkInterruptSource(interruptMask) spi_CLEAR_INTR_I2C_EC(interruptMask)
#define spi_GetI2CExtClkInterruptSource()                (spi_INTR_I2C_EC_REG)
#define spi_GetI2CExtClkInterruptMode()                  (spi_INTR_I2C_EC_MASK_REG)
#define spi_GetI2CExtClkInterruptSourceMasked()          (spi_INTR_I2C_EC_MASKED_REG)

#if (!spi_CY_SCBIP_V1)
    /* APIs to service INTR_SPI_EC register */
    #define spi_SetSpiExtClkInterruptMode(interruptMask) \
                                                                spi_WRITE_INTR_SPI_EC_MASK(interruptMask)
    #define spi_ClearSpiExtClkInterruptSource(interruptMask) \
                                                                spi_CLEAR_INTR_SPI_EC(interruptMask)
    #define spi_GetExtSpiClkInterruptSource()                 (spi_INTR_SPI_EC_REG)
    #define spi_GetExtSpiClkInterruptMode()                   (spi_INTR_SPI_EC_MASK_REG)
    #define spi_GetExtSpiClkInterruptSourceMasked()           (spi_INTR_SPI_EC_MASKED_REG)
#endif /* (!spi_CY_SCBIP_V1) */

#if(spi_SCB_MODE_UNCONFIG_CONST_CFG)
    extern void spi_SetPins(uint32 mode, uint32 subMode, uint32 uartEnableMask);
#endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */


/***************************************
*     Vars with External Linkage
***************************************/

#if (spi_SCB_IRQ_INTERNAL)
#if !defined (CY_REMOVE_spi_CUSTOM_INTR_HANDLER)
    extern cyisraddress spi_customIntrHandler;
#endif /* !defined (CY_REMOVE_spi_CUSTOM_INTR_HANDLER) */
#endif /* (spi_SCB_IRQ_INTERNAL) */

extern spi_BACKUP_STRUCT spi_backup;

#if(spi_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Common configuration variables */
    extern uint8 spi_scbMode;
    extern uint8 spi_scbEnableWake;
    extern uint8 spi_scbEnableIntr;

    /* I2C configuration variables */
    extern uint8 spi_mode;
    extern uint8 spi_acceptAddr;

    /* SPI/UART configuration variables */
    extern volatile uint8 * spi_rxBuffer;
    extern uint8   spi_rxDataBits;
    extern uint32  spi_rxBufferSize;

    extern volatile uint8 * spi_txBuffer;
    extern uint8   spi_txDataBits;
    extern uint32  spi_txBufferSize;

    /* EZI2C configuration variables */
    extern uint8 spi_numberOfAddr;
    extern uint8 spi_subAddrSize;
#endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */

#if (! (spi_SCB_MODE_I2C_CONST_CFG || \
        spi_SCB_MODE_EZI2C_CONST_CFG))
    extern uint16 spi_IntrTxMask;
#endif /* (! (spi_SCB_MODE_I2C_CONST_CFG || \
              spi_SCB_MODE_EZI2C_CONST_CFG)) */


/***************************************
*        Conditional Macro
****************************************/

#if(spi_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Defines run time operation mode */
    #define spi_SCB_MODE_I2C_RUNTM_CFG     (spi_SCB_MODE_I2C      == spi_scbMode)
    #define spi_SCB_MODE_SPI_RUNTM_CFG     (spi_SCB_MODE_SPI      == spi_scbMode)
    #define spi_SCB_MODE_UART_RUNTM_CFG    (spi_SCB_MODE_UART     == spi_scbMode)
    #define spi_SCB_MODE_EZI2C_RUNTM_CFG   (spi_SCB_MODE_EZI2C    == spi_scbMode)
    #define spi_SCB_MODE_UNCONFIG_RUNTM_CFG \
                                                        (spi_SCB_MODE_UNCONFIG == spi_scbMode)

    /* Defines wakeup enable */
    #define spi_SCB_WAKE_ENABLE_CHECK       (0u != spi_scbEnableWake)
#endif /* (spi_SCB_MODE_UNCONFIG_CONST_CFG) */

/* Defines maximum number of SCB pins */
#if (!spi_CY_SCBIP_V1)
    #define spi_SCB_PINS_NUMBER    (7u)
#else
    #define spi_SCB_PINS_NUMBER    (2u)
#endif /* (!spi_CY_SCBIP_V1) */

#endif /* (CY_SCB_PVT_spi_H) */


/* [] END OF FILE */
