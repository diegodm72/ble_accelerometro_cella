/*******************************************************************************
* File Name: on_off_ntc.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "on_off_ntc.h"

static on_off_ntc_BACKUP_STRUCT  on_off_ntc_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: on_off_ntc_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function applies only to SIO and USBIO pins.
*  It should not be called for GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet on_off_ntc_SUT.c usage_on_off_ntc_Sleep_Wakeup
*******************************************************************************/
void on_off_ntc_Sleep(void)
{
    #if defined(on_off_ntc__PC)
        on_off_ntc_backup.pcState = on_off_ntc_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            on_off_ntc_backup.usbState = on_off_ntc_CR1_REG;
            on_off_ntc_USB_POWER_REG |= on_off_ntc_USBIO_ENTER_SLEEP;
            on_off_ntc_CR1_REG &= on_off_ntc_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(on_off_ntc__SIO)
        on_off_ntc_backup.sioState = on_off_ntc_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        on_off_ntc_SIO_REG &= (uint32)(~on_off_ntc_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: on_off_ntc_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep(). This 
* function applies only to SIO and USBIO pins. It should not be called for
* GPIO or GPIO_OVT pins.
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to on_off_ntc_Sleep() for an example usage.
*******************************************************************************/
void on_off_ntc_Wakeup(void)
{
    #if defined(on_off_ntc__PC)
        on_off_ntc_PC = on_off_ntc_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            on_off_ntc_USB_POWER_REG &= on_off_ntc_USBIO_EXIT_SLEEP_PH1;
            on_off_ntc_CR1_REG = on_off_ntc_backup.usbState;
            on_off_ntc_USB_POWER_REG &= on_off_ntc_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(on_off_ntc__SIO)
        on_off_ntc_SIO_REG = on_off_ntc_backup.sioState;
    #endif
}


/* [] END OF FILE */
