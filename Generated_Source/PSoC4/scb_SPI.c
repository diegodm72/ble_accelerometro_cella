/***************************************************************************//**
* \file scb_SPI.c
* \version 4.0
*
* \brief
*  This file provides the source code to the API for the SCB Component in
*  SPI mode.
*
* Note:
*
*******************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "scb_PVT.h"
#include "scb_SPI_UART_PVT.h"

#if(scb_SCB_MODE_UNCONFIG_CONST_CFG)

    /***************************************
    *  Configuration Structure Initialization
    ***************************************/

    const scb_SPI_INIT_STRUCT scb_configSpi =
    {
        scb_SPI_MODE,
        scb_SPI_SUB_MODE,
        scb_SPI_CLOCK_MODE,
        scb_SPI_OVS_FACTOR,
        scb_SPI_MEDIAN_FILTER_ENABLE,
        scb_SPI_LATE_MISO_SAMPLE_ENABLE,
        scb_SPI_WAKE_ENABLE,
        scb_SPI_RX_DATA_BITS_NUM,
        scb_SPI_TX_DATA_BITS_NUM,
        scb_SPI_BITS_ORDER,
        scb_SPI_TRANSFER_SEPARATION,
        0u,
        NULL,
        0u,
        NULL,
        (uint32) scb_SCB_IRQ_INTERNAL,
        scb_SPI_INTR_RX_MASK,
        scb_SPI_RX_TRIGGER_LEVEL,
        scb_SPI_INTR_TX_MASK,
        scb_SPI_TX_TRIGGER_LEVEL,
        (uint8) scb_SPI_BYTE_MODE_ENABLE,
        (uint8) scb_SPI_FREE_RUN_SCLK_ENABLE,
        (uint8) scb_SPI_SS_POLARITY
    };


    /*******************************************************************************
    * Function Name: scb_SpiInit
    ****************************************************************************//**
    *
    *  Configures the scb for SPI operation.
    *
    *  This function is intended specifically to be used when the scb 
    *  configuration is set to “Unconfigured scb” in the customizer. 
    *  After initializing the scb in SPI mode using this function, 
    *  the component can be enabled using the scb_Start() or 
    * scb_Enable() function.
    *  This function uses a pointer to a structure that provides the configuration 
    *  settings. This structure contains the same information that would otherwise 
    *  be provided by the customizer settings.
    *
    *  \param config: pointer to a structure that contains the following list of 
    *   fields. These fields match the selections available in the customizer. 
    *   Refer to the customizer for further description of the settings.
    *
    *******************************************************************************/
    void scb_SpiInit(const scb_SPI_INIT_STRUCT *config)
    {
        if(NULL == config)
        {
            CYASSERT(0u != 0u); /* Halt execution due to bad function parameter */
        }
        else
        {
            /* Configure pins */
            scb_SetPins(scb_SCB_MODE_SPI, config->mode, scb_DUMMY_PARAM);

            /* Store internal configuration */
            scb_scbMode       = (uint8) scb_SCB_MODE_SPI;
            scb_scbEnableWake = (uint8) config->enableWake;
            scb_scbEnableIntr = (uint8) config->enableInterrupt;

            /* Set RX direction internal variables */
            scb_rxBuffer      =         config->rxBuffer;
            scb_rxDataBits    = (uint8) config->rxDataBits;
            scb_rxBufferSize  =         config->rxBufferSize;

            /* Set TX direction internal variables */
            scb_txBuffer      =         config->txBuffer;
            scb_txDataBits    = (uint8) config->txDataBits;
            scb_txBufferSize  =         config->txBufferSize;

            /* Configure SPI interface */
            scb_CTRL_REG     = scb_GET_CTRL_OVS(config->oversample)           |
                                            scb_GET_CTRL_BYTE_MODE(config->enableByteMode) |
                                            scb_GET_CTRL_EC_AM_MODE(config->enableWake)    |
                                            scb_CTRL_SPI;

            scb_SPI_CTRL_REG = scb_GET_SPI_CTRL_CONTINUOUS    (config->transferSeperation)  |
                                            scb_GET_SPI_CTRL_SELECT_PRECEDE(config->submode &
                                                                          scb_SPI_MODE_TI_PRECEDES_MASK) |
                                            scb_GET_SPI_CTRL_SCLK_MODE     (config->sclkMode)            |
                                            scb_GET_SPI_CTRL_LATE_MISO_SAMPLE(config->enableLateSampling)|
                                            scb_GET_SPI_CTRL_SCLK_CONTINUOUS(config->enableFreeRunSclk)  |
                                            scb_GET_SPI_CTRL_SSEL_POLARITY (config->polaritySs)          |
                                            scb_GET_SPI_CTRL_SUB_MODE      (config->submode)             |
                                            scb_GET_SPI_CTRL_MASTER_MODE   (config->mode);

            /* Configure RX direction */
            scb_RX_CTRL_REG     =  scb_GET_RX_CTRL_DATA_WIDTH(config->rxDataBits)         |
                                                scb_GET_RX_CTRL_BIT_ORDER (config->bitOrder)           |
                                                scb_GET_RX_CTRL_MEDIAN    (config->enableMedianFilter) |
                                                scb_SPI_RX_CTRL;

            scb_RX_FIFO_CTRL_REG = scb_GET_RX_FIFO_CTRL_TRIGGER_LEVEL(config->rxTriggerLevel);

            /* Configure TX direction */
            scb_TX_CTRL_REG      = scb_GET_TX_CTRL_DATA_WIDTH(config->txDataBits) |
                                                scb_GET_TX_CTRL_BIT_ORDER (config->bitOrder)   |
                                                scb_SPI_TX_CTRL;

            scb_TX_FIFO_CTRL_REG = scb_GET_TX_FIFO_CTRL_TRIGGER_LEVEL(config->txTriggerLevel);

            /* Configure interrupt with SPI handler but do not enable it */
            CyIntDisable    (scb_ISR_NUMBER);
            CyIntSetPriority(scb_ISR_NUMBER, scb_ISR_PRIORITY);
            (void) CyIntSetVector(scb_ISR_NUMBER, &scb_SPI_UART_ISR);

            /* Configure interrupt sources */
            scb_INTR_I2C_EC_MASK_REG = scb_NO_INTR_SOURCES;
            scb_INTR_SPI_EC_MASK_REG = scb_NO_INTR_SOURCES;
            scb_INTR_SLAVE_MASK_REG  = scb_GET_SPI_INTR_SLAVE_MASK(config->rxInterruptMask);
            scb_INTR_MASTER_MASK_REG = scb_GET_SPI_INTR_MASTER_MASK(config->txInterruptMask);
            scb_INTR_RX_MASK_REG     = scb_GET_SPI_INTR_RX_MASK(config->rxInterruptMask);
            scb_INTR_TX_MASK_REG     = scb_GET_SPI_INTR_TX_MASK(config->txInterruptMask);
            
            /* Configure TX interrupt sources to restore. */
            scb_IntrTxMask = LO16(scb_INTR_TX_MASK_REG);

            /* Set active SS0 */
            scb_SpiSetActiveSlaveSelect(scb_SPI_SLAVE_SELECT0);

            /* Clear RX buffer indexes */
            scb_rxBufferHead     = 0u;
            scb_rxBufferTail     = 0u;
            scb_rxBufferOverflow = 0u;

            /* Clear TX buffer indexes */
            scb_txBufferHead = 0u;
            scb_txBufferTail = 0u;
        }
    }

#else

    /*******************************************************************************
    * Function Name: scb_SpiInit
    ****************************************************************************//**
    *
    *  Configures the SCB for the SPI operation.
    *
    *******************************************************************************/
    void scb_SpiInit(void)
    {
        /* Configure SPI interface */
        scb_CTRL_REG     = scb_SPI_DEFAULT_CTRL;
        scb_SPI_CTRL_REG = scb_SPI_DEFAULT_SPI_CTRL;

        /* Configure TX and RX direction */
        scb_RX_CTRL_REG      = scb_SPI_DEFAULT_RX_CTRL;
        scb_RX_FIFO_CTRL_REG = scb_SPI_DEFAULT_RX_FIFO_CTRL;

        /* Configure TX and RX direction */
        scb_TX_CTRL_REG      = scb_SPI_DEFAULT_TX_CTRL;
        scb_TX_FIFO_CTRL_REG = scb_SPI_DEFAULT_TX_FIFO_CTRL;

        /* Configure interrupt with SPI handler but do not enable it */
    #if(scb_SCB_IRQ_INTERNAL)
            CyIntDisable    (scb_ISR_NUMBER);
            CyIntSetPriority(scb_ISR_NUMBER, scb_ISR_PRIORITY);
            (void) CyIntSetVector(scb_ISR_NUMBER, &scb_SPI_UART_ISR);
    #endif /* (scb_SCB_IRQ_INTERNAL) */

        /* Configure interrupt sources */
        scb_INTR_I2C_EC_MASK_REG = scb_SPI_DEFAULT_INTR_I2C_EC_MASK;
        scb_INTR_SPI_EC_MASK_REG = scb_SPI_DEFAULT_INTR_SPI_EC_MASK;
        scb_INTR_SLAVE_MASK_REG  = scb_SPI_DEFAULT_INTR_SLAVE_MASK;
        scb_INTR_MASTER_MASK_REG = scb_SPI_DEFAULT_INTR_MASTER_MASK;
        scb_INTR_RX_MASK_REG     = scb_SPI_DEFAULT_INTR_RX_MASK;
        scb_INTR_TX_MASK_REG     = scb_SPI_DEFAULT_INTR_TX_MASK;

        /* Configure TX interrupt sources to restore. */
        scb_IntrTxMask = LO16(scb_INTR_TX_MASK_REG);
            
        /* Set active SS0 for master */
    #if (scb_SPI_MASTER_CONST)
        scb_SpiSetActiveSlaveSelect(scb_SPI_SLAVE_SELECT0);
    #endif /* (scb_SPI_MASTER_CONST) */

    #if(scb_INTERNAL_RX_SW_BUFFER_CONST)
        scb_rxBufferHead     = 0u;
        scb_rxBufferTail     = 0u;
        scb_rxBufferOverflow = 0u;
    #endif /* (scb_INTERNAL_RX_SW_BUFFER_CONST) */

    #if(scb_INTERNAL_TX_SW_BUFFER_CONST)
        scb_txBufferHead = 0u;
        scb_txBufferTail = 0u;
    #endif /* (scb_INTERNAL_TX_SW_BUFFER_CONST) */
    }
#endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */


/*******************************************************************************
* Function Name: scb_SpiPostEnable
****************************************************************************//**
*
*  Restores HSIOM settings for the SPI master output pins (SCLK and/or SS0-SS3) 
*  to be controlled by the SCB SPI.
*
*******************************************************************************/
void scb_SpiPostEnable(void)
{
#if(scb_SCB_MODE_UNCONFIG_CONST_CFG)

    if (scb_CHECK_SPI_MASTER)
    {
    #if (scb_CTS_SCLK_PIN)
        /* Set SCB SPI to drive output pin */
        scb_SET_HSIOM_SEL(scb_CTS_SCLK_HSIOM_REG, scb_CTS_SCLK_HSIOM_MASK,
                                       scb_CTS_SCLK_HSIOM_POS, scb_CTS_SCLK_HSIOM_SEL_SPI);
    #endif /* (scb_CTS_SCLK_PIN) */

    #if (scb_RTS_SS0_PIN)
        /* Set SCB SPI to drive output pin */
        scb_SET_HSIOM_SEL(scb_RTS_SS0_HSIOM_REG, scb_RTS_SS0_HSIOM_MASK,
                                       scb_RTS_SS0_HSIOM_POS, scb_RTS_SS0_HSIOM_SEL_SPI);
    #endif /* (scb_RTS_SS0_PIN) */

    #if (scb_SS1_PIN)
        /* Set SCB SPI to drive output pin */
        scb_SET_HSIOM_SEL(scb_SS1_HSIOM_REG, scb_SS1_HSIOM_MASK,
                                       scb_SS1_HSIOM_POS, scb_SS1_HSIOM_SEL_SPI);
    #endif /* (scb_SS1_PIN) */

    #if (scb_SS2_PIN)
        /* Set SCB SPI to drive output pin */
        scb_SET_HSIOM_SEL(scb_SS2_HSIOM_REG, scb_SS2_HSIOM_MASK,
                                       scb_SS2_HSIOM_POS, scb_SS2_HSIOM_SEL_SPI);
    #endif /* (scb_SS2_PIN) */

    #if (scb_SS3_PIN)
        /* Set SCB SPI to drive output pin */
        scb_SET_HSIOM_SEL(scb_SS3_HSIOM_REG, scb_SS3_HSIOM_MASK,
                                       scb_SS3_HSIOM_POS, scb_SS3_HSIOM_SEL_SPI);
    #endif /* (scb_SS3_PIN) */
    }

#else

    #if (scb_SPI_MASTER_SCLK_PIN)
        /* Set SCB SPI to drive output pin */
        scb_SET_HSIOM_SEL(scb_SCLK_M_HSIOM_REG, scb_SCLK_M_HSIOM_MASK,
                                       scb_SCLK_M_HSIOM_POS, scb_SCLK_M_HSIOM_SEL_SPI);
    #endif /* (scb_MISO_SDA_TX_PIN_PIN) */

    #if (scb_SPI_MASTER_SS0_PIN)
        /* Set SCB SPI to drive output pin */
        scb_SET_HSIOM_SEL(scb_SS0_M_HSIOM_REG, scb_SS0_M_HSIOM_MASK,
                                       scb_SS0_M_HSIOM_POS, scb_SS0_M_HSIOM_SEL_SPI);
    #endif /* (scb_SPI_MASTER_SS0_PIN) */

    #if (scb_SPI_MASTER_SS1_PIN)
        /* Set SCB SPI to drive output pin */
        scb_SET_HSIOM_SEL(scb_SS1_M_HSIOM_REG, scb_SS1_M_HSIOM_MASK,
                                       scb_SS1_M_HSIOM_POS, scb_SS1_M_HSIOM_SEL_SPI);
    #endif /* (scb_SPI_MASTER_SS1_PIN) */

    #if (scb_SPI_MASTER_SS2_PIN)
        /* Set SCB SPI to drive output pin */
        scb_SET_HSIOM_SEL(scb_SS2_M_HSIOM_REG, scb_SS2_M_HSIOM_MASK,
                                       scb_SS2_M_HSIOM_POS, scb_SS2_M_HSIOM_SEL_SPI);
    #endif /* (scb_SPI_MASTER_SS2_PIN) */

    #if (scb_SPI_MASTER_SS3_PIN)
        /* Set SCB SPI to drive output pin */
        scb_SET_HSIOM_SEL(scb_SS3_M_HSIOM_REG, scb_SS3_M_HSIOM_MASK,
                                       scb_SS3_M_HSIOM_POS, scb_SS3_M_HSIOM_SEL_SPI);
    #endif /* (scb_SPI_MASTER_SS3_PIN) */

#endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */

    /* Restore TX interrupt sources. */
    scb_SetTxInterruptMode(scb_IntrTxMask);
}


/*******************************************************************************
* Function Name: scb_SpiStop
****************************************************************************//**
*
*  Changes the HSIOM settings for the SPI master output pins 
*  (SCLK and/or SS0-SS3) to keep them inactive after the block is disabled. 
*  The output pins are controlled by the GPIO data register.
*
*******************************************************************************/
void scb_SpiStop(void)
{
#if(scb_SCB_MODE_UNCONFIG_CONST_CFG)

    if (scb_CHECK_SPI_MASTER)
    {
    #if (scb_CTS_SCLK_PIN)
        /* Set output pin state after block is disabled */
        scb_uart_cts_spi_sclk_Write(scb_GET_SPI_SCLK_INACTIVE);

        /* Set GPIO to drive output pin */
        scb_SET_HSIOM_SEL(scb_CTS_SCLK_HSIOM_REG, scb_CTS_SCLK_HSIOM_MASK,
                                       scb_CTS_SCLK_HSIOM_POS, scb_CTS_SCLK_HSIOM_SEL_GPIO);
    #endif /* (scb_uart_cts_spi_sclk_PIN) */

    #if (scb_RTS_SS0_PIN)
        /* Set output pin state after block is disabled */
        scb_uart_rts_spi_ss0_Write(scb_GET_SPI_SS0_INACTIVE);

        /* Set GPIO to drive output pin */
        scb_SET_HSIOM_SEL(scb_RTS_SS0_HSIOM_REG, scb_RTS_SS0_HSIOM_MASK,
                                       scb_RTS_SS0_HSIOM_POS, scb_RTS_SS0_HSIOM_SEL_GPIO);
    #endif /* (scb_uart_rts_spi_ss0_PIN) */

    #if (scb_SS1_PIN)
        /* Set output pin state after block is disabled */
        scb_spi_ss1_Write(scb_GET_SPI_SS1_INACTIVE);

        /* Set GPIO to drive output pin */
        scb_SET_HSIOM_SEL(scb_SS1_HSIOM_REG, scb_SS1_HSIOM_MASK,
                                       scb_SS1_HSIOM_POS, scb_SS1_HSIOM_SEL_GPIO);
    #endif /* (scb_SS1_PIN) */

    #if (scb_SS2_PIN)
        /* Set output pin state after block is disabled */
        scb_spi_ss2_Write(scb_GET_SPI_SS2_INACTIVE);

        /* Set GPIO to drive output pin */
        scb_SET_HSIOM_SEL(scb_SS2_HSIOM_REG, scb_SS2_HSIOM_MASK,
                                       scb_SS2_HSIOM_POS, scb_SS2_HSIOM_SEL_GPIO);
    #endif /* (scb_SS2_PIN) */

    #if (scb_SS3_PIN)
        /* Set output pin state after block is disabled */
        scb_spi_ss3_Write(scb_GET_SPI_SS3_INACTIVE);

        /* Set GPIO to drive output pin */
        scb_SET_HSIOM_SEL(scb_SS3_HSIOM_REG, scb_SS3_HSIOM_MASK,
                                       scb_SS3_HSIOM_POS, scb_SS3_HSIOM_SEL_GPIO);
    #endif /* (scb_SS3_PIN) */
    
        /* Store TX interrupt sources (exclude level triggered) for master. */
        scb_IntrTxMask = LO16(scb_GetTxInterruptMode() & scb_INTR_SPIM_TX_RESTORE);
    }
    else
    {
        /* Store TX interrupt sources (exclude level triggered) for slave. */
        scb_IntrTxMask = LO16(scb_GetTxInterruptMode() & scb_INTR_SPIS_TX_RESTORE);
    }

#else

#if (scb_SPI_MASTER_SCLK_PIN)
    /* Set output pin state after block is disabled */
    scb_sclk_m_Write(scb_GET_SPI_SCLK_INACTIVE);

    /* Set GPIO to drive output pin */
    scb_SET_HSIOM_SEL(scb_SCLK_M_HSIOM_REG, scb_SCLK_M_HSIOM_MASK,
                                   scb_SCLK_M_HSIOM_POS, scb_SCLK_M_HSIOM_SEL_GPIO);
#endif /* (scb_MISO_SDA_TX_PIN_PIN) */

#if (scb_SPI_MASTER_SS0_PIN)
    /* Set output pin state after block is disabled */
    scb_ss0_m_Write(scb_GET_SPI_SS0_INACTIVE);

    /* Set GPIO to drive output pin */
    scb_SET_HSIOM_SEL(scb_SS0_M_HSIOM_REG, scb_SS0_M_HSIOM_MASK,
                                   scb_SS0_M_HSIOM_POS, scb_SS0_M_HSIOM_SEL_GPIO);
#endif /* (scb_SPI_MASTER_SS0_PIN) */

#if (scb_SPI_MASTER_SS1_PIN)
    /* Set output pin state after block is disabled */
    scb_ss1_m_Write(scb_GET_SPI_SS1_INACTIVE);

    /* Set GPIO to drive output pin */
    scb_SET_HSIOM_SEL(scb_SS1_M_HSIOM_REG, scb_SS1_M_HSIOM_MASK,
                                   scb_SS1_M_HSIOM_POS, scb_SS1_M_HSIOM_SEL_GPIO);
#endif /* (scb_SPI_MASTER_SS1_PIN) */

#if (scb_SPI_MASTER_SS2_PIN)
    /* Set output pin state after block is disabled */
    scb_ss2_m_Write(scb_GET_SPI_SS2_INACTIVE);

    /* Set GPIO to drive output pin */
    scb_SET_HSIOM_SEL(scb_SS2_M_HSIOM_REG, scb_SS2_M_HSIOM_MASK,
                                   scb_SS2_M_HSIOM_POS, scb_SS2_M_HSIOM_SEL_GPIO);
#endif /* (scb_SPI_MASTER_SS2_PIN) */

#if (scb_SPI_MASTER_SS3_PIN)
    /* Set output pin state after block is disabled */
    scb_ss3_m_Write(scb_GET_SPI_SS3_INACTIVE);

    /* Set GPIO to drive output pin */
    scb_SET_HSIOM_SEL(scb_SS3_M_HSIOM_REG, scb_SS3_M_HSIOM_MASK,
                                   scb_SS3_M_HSIOM_POS, scb_SS3_M_HSIOM_SEL_GPIO);
#endif /* (scb_SPI_MASTER_SS3_PIN) */

    #if (scb_SPI_MASTER_CONST)
        /* Store TX interrupt sources (exclude level triggered). */
        scb_IntrTxMask = LO16(scb_GetTxInterruptMode() & scb_INTR_SPIM_TX_RESTORE);
    #else
        /* Store TX interrupt sources (exclude level triggered). */
        scb_IntrTxMask = LO16(scb_GetTxInterruptMode() & scb_INTR_SPIS_TX_RESTORE);
    #endif /* (scb_SPI_MASTER_CONST) */

#endif /* (scb_SCB_MODE_UNCONFIG_CONST_CFG) */
}


#if (scb_SPI_MASTER_CONST)
    /*******************************************************************************
    * Function Name: scb_SetActiveSlaveSelect
    ****************************************************************************//**
    *
    *  Selects one of the four slave select lines to be active during the transfer.
    *  After initialization the active slave select line is 0.
    *  The component should be in one of the following states to change the active
    *  slave select signal source correctly:
    *   - The component is disabled.
    *   - The component has completed transfer (TX FIFO is empty and the
    *     SCB_INTR_MASTER_SPI_DONE status is set).
    *
    *  This function does not check that these conditions are met.
    *  This function is only applicable to SPI Master mode of operation.
    *
    *  \param slaveSelect: slave select line which will be active while the following
    *   transfer.
    *   - scb_SPI_SLAVE_SELECT0 - Slave select 0.
    *   - scb_SPI_SLAVE_SELECT1 - Slave select 1.
    *   - scb_SPI_SLAVE_SELECT2 - Slave select 2.
    *   - scb_SPI_SLAVE_SELECT3 - Slave select 3.
    *
    *******************************************************************************/
    void scb_SpiSetActiveSlaveSelect(uint32 slaveSelect)
    {
        uint32 spiCtrl;

        spiCtrl = scb_SPI_CTRL_REG;

        spiCtrl &= (uint32) ~scb_SPI_CTRL_SLAVE_SELECT_MASK;
        spiCtrl |= (uint32)  scb_GET_SPI_CTRL_SS(slaveSelect);

        scb_SPI_CTRL_REG = spiCtrl;
    }
#endif /* (scb_SPI_MASTER_CONST) */


#if !(scb_CY_SCBIP_V0 || scb_CY_SCBIP_V1)
    /*******************************************************************************
    * Function Name: scb_SpiSetSlaveSelectPolarity
    ****************************************************************************//**
    *
    *  Sets active polarity for slave select line.
    *  The component should be in one of the following states to change the active
    *  slave select signal source correctly:
    *   - The component is disabled.
    *   - The component has completed transfer.
    *  
    *  This function does not check that these conditions are met.
    *
    *  \param slaveSelect: slave select line to change active polarity.
    *   - scb_SPI_SLAVE_SELECT0 - Slave select 0.
    *   - scb_SPI_SLAVE_SELECT1 - Slave select 1.
    *   - scb_SPI_SLAVE_SELECT2 - Slave select 2.
    *   - scb_SPI_SLAVE_SELECT3 - Slave select 3.
    *
    *  \param polarity: active polarity of slave select line.
    *   - scb_SPI_SS_ACTIVE_LOW  - Slave select is active low.
    *   - scb_SPI_SS_ACTIVE_HIGH - Slave select is active high.
    *
    *******************************************************************************/
    void scb_SpiSetSlaveSelectPolarity(uint32 slaveSelect, uint32 polarity)
    {
        uint32 ssPolarity;

        /* Get position of the polarity bit associated with slave select line */
        ssPolarity = scb_GET_SPI_CTRL_SSEL_POLARITY((uint32) 1u << slaveSelect);

        if (0u != polarity)
        {
            scb_SPI_CTRL_REG |= (uint32)  ssPolarity;
        }
        else
        {
            scb_SPI_CTRL_REG &= (uint32) ~ssPolarity;
        }
    }
#endif /* !(scb_CY_SCBIP_V0 || scb_CY_SCBIP_V1) */


#if(scb_SPI_WAKE_ENABLE_CONST)
    /*******************************************************************************
    * Function Name: scb_SpiSaveConfig
    ****************************************************************************//**
    *
    *  Clears INTR_SPI_EC.WAKE_UP and enables it. This interrupt
    *  source triggers when the master assigns the SS line and wakes up the device.
    *
    *******************************************************************************/
    void scb_SpiSaveConfig(void)
    {
        /* Clear and enable SPI wakeup interrupt source */
        scb_ClearSpiExtClkInterruptSource(scb_INTR_SPI_EC_WAKE_UP);
        scb_SetSpiExtClkInterruptMode(scb_INTR_SPI_EC_WAKE_UP);
    }


    /*******************************************************************************
    * Function Name: scb_SpiRestoreConfig
    ****************************************************************************//**
    *
    *  Disables the INTR_SPI_EC.WAKE_UP interrupt source. After wakeup
    *  slave does not drive the MISO line and the master receives 0xFF.
    *
    *******************************************************************************/
    void scb_SpiRestoreConfig(void)
    {
        /* Disable SPI wakeup interrupt source */
        scb_SetSpiExtClkInterruptMode(scb_NO_INTR_SOURCES);
    }
#endif /* (scb_SPI_WAKE_ENABLE_CONST) */


/* [] END OF FILE */
