/***************************************************************************//**
* \file spi_SPI_UART_PVT.h
* \version 4.0
*
* \brief
*  This private file provides constants and parameter values for the
*  SCB Component in SPI and UART modes.
*  Please do not use this file or its content in your project.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_SPI_UART_PVT_spi_H)
#define CY_SCB_SPI_UART_PVT_spi_H

#include "spi_SPI_UART.h"


/***************************************
*     Internal Global Vars
***************************************/

#if (spi_INTERNAL_RX_SW_BUFFER_CONST)
    extern volatile uint32  spi_rxBufferHead;
    extern volatile uint32  spi_rxBufferTail;
    
    /**
    * \addtogroup group_globals
    * @{
    */
    
    /** Sets when internal software receive buffer overflow
     *  was occurred.
    */  
    extern volatile uint8   spi_rxBufferOverflow;
    /** @} globals */
#endif /* (spi_INTERNAL_RX_SW_BUFFER_CONST) */

#if (spi_INTERNAL_TX_SW_BUFFER_CONST)
    extern volatile uint32  spi_txBufferHead;
    extern volatile uint32  spi_txBufferTail;
#endif /* (spi_INTERNAL_TX_SW_BUFFER_CONST) */

#if (spi_INTERNAL_RX_SW_BUFFER)
    extern volatile uint8 spi_rxBufferInternal[spi_INTERNAL_RX_BUFFER_SIZE];
#endif /* (spi_INTERNAL_RX_SW_BUFFER) */

#if (spi_INTERNAL_TX_SW_BUFFER)
    extern volatile uint8 spi_txBufferInternal[spi_TX_BUFFER_SIZE];
#endif /* (spi_INTERNAL_TX_SW_BUFFER) */


/***************************************
*     Private Function Prototypes
***************************************/

void spi_SpiPostEnable(void);
void spi_SpiStop(void);

#if (spi_SCB_MODE_SPI_CONST_CFG)
    void spi_SpiInit(void);
#endif /* (spi_SCB_MODE_SPI_CONST_CFG) */

#if (spi_SPI_WAKE_ENABLE_CONST)
    void spi_SpiSaveConfig(void);
    void spi_SpiRestoreConfig(void);
#endif /* (spi_SPI_WAKE_ENABLE_CONST) */

void spi_UartPostEnable(void);
void spi_UartStop(void);

#if (spi_SCB_MODE_UART_CONST_CFG)
    void spi_UartInit(void);
#endif /* (spi_SCB_MODE_UART_CONST_CFG) */

#if (spi_UART_WAKE_ENABLE_CONST)
    void spi_UartSaveConfig(void);
    void spi_UartRestoreConfig(void);
#endif /* (spi_UART_WAKE_ENABLE_CONST) */


/***************************************
*         UART API Constants
***************************************/

/* UART RX and TX position to be used in spi_SetPins() */
#define spi_UART_RX_PIN_ENABLE    (spi_UART_RX)
#define spi_UART_TX_PIN_ENABLE    (spi_UART_TX)

/* UART RTS and CTS position to be used in  spi_SetPins() */
#define spi_UART_RTS_PIN_ENABLE    (0x10u)
#define spi_UART_CTS_PIN_ENABLE    (0x20u)


/***************************************
* The following code is DEPRECATED and
* must not be used.
***************************************/

/* Interrupt processing */
#define spi_SpiUartEnableIntRx(intSourceMask)  spi_SetRxInterruptMode(intSourceMask)
#define spi_SpiUartEnableIntTx(intSourceMask)  spi_SetTxInterruptMode(intSourceMask)
uint32  spi_SpiUartDisableIntRx(void);
uint32  spi_SpiUartDisableIntTx(void);


#endif /* (CY_SCB_SPI_UART_PVT_spi_H) */


/* [] END OF FILE */
