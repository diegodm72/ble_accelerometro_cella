/*******************************************************************************
* File Name: cs_ee.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "cs_ee.h"

static cs_ee_BACKUP_STRUCT  cs_ee_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: cs_ee_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function applies only to SIO and USBIO pins.
*  It should not be called for GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet cs_ee_SUT.c usage_cs_ee_Sleep_Wakeup
*******************************************************************************/
void cs_ee_Sleep(void)
{
    #if defined(cs_ee__PC)
        cs_ee_backup.pcState = cs_ee_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            cs_ee_backup.usbState = cs_ee_CR1_REG;
            cs_ee_USB_POWER_REG |= cs_ee_USBIO_ENTER_SLEEP;
            cs_ee_CR1_REG &= cs_ee_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(cs_ee__SIO)
        cs_ee_backup.sioState = cs_ee_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        cs_ee_SIO_REG &= (uint32)(~cs_ee_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: cs_ee_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep(). This 
* function applies only to SIO and USBIO pins. It should not be called for
* GPIO or GPIO_OVT pins.
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to cs_ee_Sleep() for an example usage.
*******************************************************************************/
void cs_ee_Wakeup(void)
{
    #if defined(cs_ee__PC)
        cs_ee_PC = cs_ee_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            cs_ee_USB_POWER_REG &= cs_ee_USBIO_EXIT_SLEEP_PH1;
            cs_ee_CR1_REG = cs_ee_backup.usbState;
            cs_ee_USB_POWER_REG &= cs_ee_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(cs_ee__SIO)
        cs_ee_SIO_REG = cs_ee_backup.sioState;
    #endif
}


/* [] END OF FILE */
